lua脚本接口说明模板
==============
> 目录

<!-- TOC -->

- [1. 文档说明和修改记录](#1-文档说明和修改记录)
    - [1.1. 版本：v1.0](#11-版本v10)
    - [1.2. 版本：v1.1](#12-版本v11)
    - [1.3. 版本：v1.2](#13-版本v12)
    - [1.4. 版本：v1.3](#14-版本v13)
    - [1.5. 版本：v1.4](#15-版本v14)
    - [1.6. 版本 2017-06-13](#16-版本-2017-06-13)
    - [1.7. 版本2017-7-20](#17-版本2017-7-20)
- [2. 系统变量和系统函数](#2-系统变量和系统函数)
    - [2.1. 概念](#21-概念)
    - [2.2. IO系统变量](#22-io系统变量)
    - [2.3. 系统函数](#23-系统函数)
- [3. 系统函数说明](#3-系统函数说明)
    - [3.1. 初始化类](#31-初始化类)
        - [3.1.1. 打开伺服使能](#311-打开伺服使能)
        - [3.1.2. 停止伺服](#312-停止伺服)
        - [3.1.3. 退出脚本](#313-退出脚本)
        - [3.1.4. 设置机械臂类型](#314-设置机械臂类型)
    - [3.2. 用户日志消息类函数](#32-用户日志消息类函数)
        - [3.2.1. 用户报告错误](#321-用户报告错误)
        - [3.2.2. 用户报告警告](#322-用户报告警告)
        - [3.2.3. 用户报告日志](#323-用户报告日志)
    - [3.3. 运动控制类](#33-运动控制类)
        - [3.3.1. 点到点连续轨迹参数为pose运动](#331-点到点连续轨迹参数为pose运动)
        - [3.3.2. 点到点单指令运动参数为pose运动](#332-点到点单指令运动参数为pose运动)
        - [3.3.3. 点到点连续轨迹参数为posit运动](#333-点到点连续轨迹参数为posit运动)
        - [3.3.4. 点到点单指令参数为posit运动](#334-点到点单指令参数为posit运动)
        - [3.3.5. 等待运动结束](#335-等待运动结束)
    - [3.4. IO板输入输出](#34-io板输入输出)
        - [3.4.1. 设置数字IO输出 DO](#341-设置数字io输出-do)
        - [3.4.2. 读取数字IO输入 DI](#342-读取数字io输入-di)
        - [3.4.3. 设置DA输出](#343-设置da输出)
        - [3.4.4. 设置AD输入模式](#344-设置ad输入模式)
        - [3.4.5. 读取AD输入](#345-读取ad输入)
        - [3.4.6. 设置DOF输出](#346-设置dof输出)
        - [3.4.7. 设置DOV输出](#347-设置dov输出)
    - [3.5. 其他函数](#35-其他函数)
        - [3.5.1. 毫秒延时等待](#351-毫秒延时等待)
        - [3.5.2. 秒延时等待](#352-秒延时等待)
        - [3.5.3. 设置参考坐标系](#353-设置参考坐标系)
        - [3.5.4. 脚本例程](#354-脚本例程)
        - [3.5.5. 例程1](#355-例程1)
    - [3.6. 机械臂连接](#36-机械臂连接)
        - [3.6.1. 设置VIO](#361-设置vio)
        - [3.6.2. 读取VIO](#362-读取vio)
        - [3.6.3. 创建和其他机械臂链接的context](#363-创建和其他机械臂链接的context)
        - [3.6.4. 断开和其他机械臂链接的context](#364-断开和其他机械臂链接的context)
- [4. 对主控器API说明](#4-对主控器api说明)
    - [4.1. 回调函数](#41-回调函数)
    - [4.2. 回调函数消息类型](#42-回调函数消息类型)

<!-- /TOC -->

# 1. 文档说明和修改记录
* 功能：   
1. 在lua5.3.4开源代码基础上编译成windwos和linux动态库或静态库   
2. 提供io和driver输入、输出接口   
3. 提供目录已有的脚本查询接口   
4. 目录下脚本的运行接口    
5. 修改原有的脚本执行日志输出接口

## 1.1. 版本：v1.0
* 修改时间：
* 修改内容概要：

## 1.2. 版本：v1.1
* 修改时间：2017-03-20
* 修改内容概要：   
1. 添加2.3.5 等待运动结束API接口     
2. 添加2.4.3 设置DA输出  
3. 添加2.4.4 设置AD采样输入     
4. 添加2.4.5 设置DOF输出

## 1.3. 版本：v1.2
* 修改时间：2017-03-24
* 修改内容概要：  
1. 添加回调函数接口及使用方法
2. 修改2.4.4读取AD采样值为设置AD输出
3. 添加2.4.5读取AD采样值
4. 添加2.4.7设置DOV输出
5. 修改函数名中digital为di/do
6. 修改函数名中adc-->ad，dac--->da
7. 返回值中true--->1, false--->0

## 1.4. 版本：v1.3
* 修改时间：2017-04-19
* 修改内容概要：  
1. 用C语言重构lua模块
2. 单独开一个常驻线程,负责lua解析及状态管理,实现启动,暂停,继续,停止,简单断点功能
3. 重新定义中间层回调功能,实现解决一些执行周期长的操作中,状态管理不能的问题
4. 修改一些lua的robot回调函数名
5. 修改示例代码中的错误
6. 增加一个完整函数使用示例脚本
7. 增加比较完整的主控回调示例

## 1.5. 版本：v1.4
* 修改时间：2017-04-24
* 修改内容概要：  
1. 增加lua消息回调 LUA_MSG_QUEUE_SERVO_DONE、LUA_MSG_QUEUE_MOTION_PATH_BUFF_FULL和LUA_MSG_QUEUE_MOTION_DONE

## 1.6. 版本 2017-06-13 
1. 修改章节3.4.1和3.4.2，脚本的IO读取和设置
2. 修改set_mio_dof/set_gio_dof,根据mode拆分为多个函数 (未完成)
3. 修改set_mio_ad,sample_mode和sample_data的数据类型 (未完成)
4. 修改章节3.2.3,用户报告日志，修改函数名称

## 1.7. 版本2017-7-20
1. 添加VIO的设置和读取，章节3.6.1和3.6.2
2. 添加机械臂链接创建,章节3.6.3
3. 添加机械臂链接断开,章节3.6.4


# 2. 系统变量和系统函数

## 2.1. 概念
* 系统变量：指的根据外部设备需求固有的一些lua变量。
* 系统函数：指的lua脚本执行器，给用户提供的一些接口函数。

## 2.2. IO系统变量
* 待定！！！
    | index | 变量名 | 说明 | 
    | ---- | ---- | ---- | 
     1 | DO1 | 数字输出通道1 | 
     2 | DI1 | 数字输入通道1 | 
     3 |  |  |
## 2.3. 系统函数     
* 使能伺服：robot.servo_enable(table enable)
* 关闭伺服：robot.servo_stop()
* 退出脚本：robot.exit()

# 3. 系统函数说明

## 3.1. 初始化类
* 打开伺服使能
* 停止伺服
* 退出脚本
* 设置机械臂类型

------------------------------------------------
### 3.1.1. 打开伺服使能
* 函数名称：robot.servo_enable(table enable)
* 参数： table enable 四/六轴兼容 table 为6个值 四轴时后两个不用默认写0
* 返回值：    
    1->初始化成功     
    0->初始化失败，自动退出脚本
* 功能：关节伺服使能，并自动找机械位置
* 例：
```lua
tbl = {["j1"] = 0, ["j2"] = 1, ["j3"] = 0,["j4"] = 1,["j5"] = 0,["j6"] = 0}
a = robot.servo_enable（tab1）
if(a)
then 
    robot.report_log("servo enable done"); 
else 
    robot.report_error("servo enable faild");
end
```
------------------------------------------------
### 3.1.2. 停止伺服
* 函数名称：robot.servo_stop()
* 参数： 无
* 返回值：无
* 功能：关闭伺服使能
* 注意：
    1、脚本能继续运行，但是在没有使能伺服之前，
    后面脚本命令如果与运动相关则会发生错误。并退出脚本
* 例：
```lua
if(a)
then 
    robot.report_log("servo enable done"); 
else 
    robot.report_error("servo enable done");
end
robot.servo_stop()
```
------------------------------------------------
### 3.1.3. 退出脚本
* 函数名称：robot.exit()
* 参数： 无
* 返回值：无
* 功能：退出脚本
* 例：
```lua
if（a）
then
robot.exit()
end
```
------------------------------------------------

### 3.1.4. 设置机械臂类型
* 函数名称：robot.set_robot_struct(string robot,string mio_id,string gio_id)
* 参数：   
    robot->具体参数含义暂定
	mio_id-> 扩展IO的ID号,具体值未定
	gio_id-> 工具IO的ID号,具体值未定
* 返回值：   
    1->设置成功
    0->设置失败
* 功能：退出脚本
* 例：
```lua
-- 设置机械臂为scara_1
robot.set_robot_struct("scara_1")

```
------------------------------------------------

## 3.2. 用户日志消息类函数
* 完成对UI的弹窗等功能

### 3.2.1. 用户报告错误
* 函数名称：robot.report_error(string error_msg)
* 参数： string error_msg
* 返回值：无
* 功能：用户报告错误消息
* 例：
```lua
if(a)
then 
    robot.report_log("servo enable done"); 
else 
    robot.report_error("servo enable done");
end
```
------------------------------------------------
### 3.2.2. 用户报告警告
* 函数名称：robot.report_warn(string warn_msg)
* 参数： string error_msg
* 返回值：无
* 功能：用户报告错误消息
* 例：
```lua
if(a)
then 
    robot.report_warn("report_warn"); 
else 
    robot.report_error("servo enable done");
end
```
------------------------------------------------
### 3.2.3. 用户报告日志
* 函数名称：robot.report_info(string error_msg)
* 参数： string error_msg
* 返回值：无
* 功能：用户报告错误消息
* 例：
```lua
if(a)
then 
    robot.report_info("servo enable done"); 
else 
    robot.report_info("servo enable done");
end
```
------------------------------------------------

## 3.3. 运动控制类
* 完成各类运动控制的功能

### 3.3.1. 点到点连续轨迹参数为pose运动
* 函数名称：robot.movep_cp_pose(table pose,number ik_flag,table user_vel, table user_ovl,number enable)
* 参数：  
    pose->位姿参数   
    ik_flag->运动学逆解姿态 0：右臂 1：左臂   
    user_vel->用户速度参数   
    user_ovl->用户逼近参数   
    enable->有效使能   
* 返回值：   
    1->初始化成功   
    0->运动添加失败
* 功能：点到点连续轨迹参数为pose运动，
* 注意：
    1、连续轨迹运动会进入速度前瞻和逼近buff    
    2、所以脚本执行完这条命令并不代表已经运动到该位置    
    3、想要在连续轨迹模式下运行完该函数到达指定位置，在该函数后加入robot.wait_move_finished()
* 例：
```lua
-- pose姿态：x,y,z为笛卡尔坐标位置，rx,ry,rz为rpy姿态（回转角、俯仰角、偏转角）
pose = {["x"] = 400, ["y"] = 0, ["z"] = 0,["rx"] = 0,["ry"] = 0,["rz"] = 0}
-- user_vel用户速度参数：j_vel,j_acc,j_dec,j_jerk分别为关节的目标速度、加速度、减速度、jerk(单位1，取值0.0001-1)
-- l_vel,l_acc,l_dec,l_jerk分别为曲线运动的线性速度参数(基本单位为：mm，取值0-100mm)
-- r_vel,r_acc,r_dec,r_jerk分别为rpy姿态旋转的转动速度(基本单位为：rad，取值0-0.5rad)
user_vel = {
    ["j_vel"] = 0.9, ["j_acc"] = 0.9, ["j_dec"] = 0.9, ["j_jerk"] = 0.9,
    ["l_vel"] = 100, ["l_acc"] = 100, ["l_dec"] = 0.9, ["l_jerk"] = 0.9,
    ["r_vel"] = 1.2, ["r_acc"] = 1.2, ["r_dec"] = 1.2, ["r_jerk"] = 1.2}
-- user_ovl用户逼近参数：rel为相对逼近参数（单位1，取值0-1），
user_ovl = {["rel"] = 0.5,["abs"] = 10,["rad"] = 0.1}
robot.movep_cp_pose(pose,0,user_vel,user_ovl,1)
-- 等待之前所有的运动结束
robot.wait_move_finished()
robot.report_log("move done"); 
```
------------------------------------------------
### 3.3.2. 点到点单指令运动参数为pose运动
* 函数名称：robot.movep_sp_pose(stable pose,number ik_flag,table user_vel, number enable)
* 参数：  
    pose->位姿参数   
    ik_flag->运动学逆解姿态 0：右臂 1：左臂   
    user_vel->用户速度参数   
    enable->有效使能   
* 返回值：   
    1->初始化成功   
    0->运动添加失败
* 功能：点到点单指令运动参数为pose运动
* 注意：
    1、该函数只有在当前运动结束后才会退出    
* 例：
```lua
-- 除ovl参数外同robot.movep_sp_pose
robot.movep_sp_pose(pose,0,user_vel,1)
robot.report_log("move done"); 
```
------------------------------------------------
### 3.3.3. 点到点连续轨迹参数为posit运动
* 函数名称：robot.movep_cp_posit(table posit,table user_vel,table user_ovl, number enable)
* 参数：  
    posit->关节位置参数  
    user_vel->用户速度参数   
    user_ovl->用户逼近参数   
    enable->有效使能   
* 返回值：   
    1->初始化成功   
    0->运动添加失败
* 功能：点到点连续轨迹参数为posit运动
* 注意：
    1、同 robot.movep_cp_pose   
* 例：
```lua
-- posit关节位置：j1-j6分别为关节1到6
posit = {["j1"] = 0, ["j2"] = 0, ["j3"] = 0,["j4"] = 0,["j5"] = 0,["j6"] = 0}
robot.movep_cp_posit(posit,user_vel,user_ovl,1)
-- 等待之前所有的运动结束
robot.wait_move_finished()
robot.report_log("move done"); 
```
------------------------------------------------
### 3.3.4. 点到点单指令参数为posit运动
* 函数名称：robot.movep_sp_posit(stable posit,table user_vel,number enable)
* 参数：  
    posit->关节位置参数  
    user_vel->用户速度参数   
    enable->有效使能   
* 返回值：   
    1->运动完成   
    0->运动失败
* 功能：点到点单指令参数为posit运动
* 注意：
    1、同 robot.movep_sp_pose   
* 例：
```lua
-- posit关节位置：j1-j6分别为关节1到6
posit = {["j1"] = 0, ["j2"] = 0, ["j3"] = 0,["j4"] = 0,["j5"] = 0,["j6"] = 0}
robot.movep_sp_posit(posit,user_ovl,1)
robot.report_log("move done"); 
```
------------------------------------------------

### 3.3.5. 等待运动结束
* 函数名称：robot.wait_move_finished()
* 参数: 无
* 返回值：   
    1->运动已完成   
    0->运动添加失败，自动退出脚本
* 功能：点到点连续轨迹参数为pose运动，
* 注意：
* 例：
```lua
robot.movep_cp_pose(pose,0,user_vel,user_ovl,1)
-- 等待之前所有的运动结束
robot.wait_move_finished()
```
------------------------------------------------

## 3.4. IO板输入输出
* 完成IO输出和读取输入任务

### 3.4.1. 设置数字IO输出 DO
* 函数名称1：robot.set_mio_digital(number channel,number state) 扩展IO板
* 函数名称2：robot.set_gio_digital(number channel,number state) 工具IO板
* 参数：  
    channel->通道号 1：通道1，32：通道32     
    state->输出状态 1:打开 0：关闭  
* 返回值：   
    1->设置数字IO输出成功   
    0-设置数字IO输出失败
* 功能：设置数字IO输出
* 注意：无 
* 例：
```lua
-- 通道1，打开
robot.set_mio_do（1,1）;
```
------------------------------------------------

### 3.4.2. 读取数字IO输入 DI
* 函数名称1：robot.read_mio_digital(number channel)
* 函数名称2：robot.read_gio_digital(number channel)
* 参数：  
    channel->通道号 1：通道1，32：通道32   
* 返回值1：   
    0->读取到该通道输入为0，即关闭状态
    1->读取到该通道输入为1，即打开状态
* 返回值2（无，读取失败自动退出脚本，采用一个返回值）:   
    /*0->读取失败（通道错误，或者该IO板未连接），自动退出脚本*/   
    /*1->读取成功*/
* 功能：读取数字IO输入
* 注意：无
* 例：
```lua
-- 读取数字输入通道1
a = robot.read_mio_digital（1）;
print(a)
```
------------------------------------------------

### 3.4.3. 设置DA输出
* 函数名称1：robot.set_mio_da(number group,number mode,number data)
* 函数名称2：robot.set_gio_da(number group,number mode,number data)
* 参数：   
    group->组号 1:组1 2:组2    
    mode->模式 0：电流输出(单位mA) 1：电压输出模式(单位V)
    data->电流值，电压值 
* 返回值1：   
    0->设置失败
    1->设置成功
* 功能：设置DA输出
* 注意：无
* 例：
```lua
--设置扩展IO板DA输出，组别为第1组，输出模式为电流输出，输出数值为20.5mA
a = robot.set_mio_da(1,0,20.5);
print(a)
```

------------------------------------------------

### 3.4.4. 设置AD输入模式
* 函数名称1：robot.set_mio_ad(number channel,number sample_mode,number trigger_mode, number trigger_data)
* 函数名称2：robot.set_gio_ad(number channel,number sample_mode,number trigger_mode, number trigger_data)
* 参数：  
    channel->通道号1：通道1，8：通道8       
    sample_mode->采样模式 0:电流模式；1：电压模式(0-5V) 2:电压模式(0-10V)    
    trigger_mode->触发模式: 0关闭；1->大于等于触发 2->小于等于触发   
    trigger_data->触发值:电流模式为mA,电压模式为V
* 返回值1：   
    0->设置失败
    1->设置成功
* 功能：设置AD输入模式
* 注意：无
* 例：
```lua
--设置扩展IO板AD输入模式,电流采样模式，触发关闭，触发值为0
ret = robot.set_mio_ad(1,0,0,0);
print(ret)
```
------------------------------------------------

### 3.4.5. 读取AD输入
* 函数名称1：robot.read_mio_ad(number channel)
* 函数名称2：robot.read_gio_ad(number channel)
* 参数：  
    channel->通道号1：通道1，8：通道8       
* 返回值：   
    number: AD采样值, 少于0为读取错误
* 功能：设置AD输入模式
* 注意：无
* 例：
```lua
--读取扩展IO板AD采样值
ad_data = robot.read_mio_ad(1);
print(ad_data)
```
------------------------------------------------

### 3.4.6. 设置DOF输出
* 函数名称1：robot.set_mio_dof(number channel,number mode,number data2,number data3,number freq)
* 函数名称2：robot.set_gio_dof(number channel,number mode,number data2,number data3,number freq)
* 参数：  
    channel->通道号 1：通道1，8：通道8       
    mode->模式 0：普通IO  1：脉冲输出(频率输出) 2：PWM输出    
    data2->在频率输出模式表示，脉冲个数(0:表示无限制,有限个数时要输出完成后才能切换成其他输出模式);   
        在PWM输出模式表示占空比(0-100); 普通IO模式无效;   
    data3->在普通IO模式下，1->打开，0->关闭；其他模式无效
	freq->输出频率 1-1000HZ
* 返回值1：   
    0->设置失败   
    1->设置成功
* 功能：设置DOF输出
* 注意：无
* 例：
```lua
--设置扩展IO板DOF输出，通道1输出模式为频率输出，输出100个脉冲信号
a = robot.set_mio_dof(1,1,100,0,1000);
print(a)
```

------------------------------------------------

### 3.4.7. 设置DOV输出
* 函数名称1：robot.set_mio_dov(number channel,number mode)
* 函数名称2：robot.set_gio_dov(number channel,number mode)
* 参数：     
    channel->通道号1：通道1，8：通道8       
    mode->模式 0->关闭，1->12V，2->24V    
* 返回值1：   
    0->设置失败   
    1->设置成功
* 功能：设置DOV输出
* 注意：无
* 例：
```lua
--设置扩展IO板DOV输出，通道1输出模式为12V输出
a = robot.set_mio_dov(1,1);
print(a)
```

------------------------------------------------

## 3.5. 其他函数
* 部分需要添加的相关系统函数

### 3.5.1. 毫秒延时等待
* 函数名称：robot.delay_ms(number ms)
* 参数：  
    ms->等待ms数    
* 返回值：无
* 功能：毫秒延时等待
* 注意：无
* 例：
```lua
-- 等待100ms
robot.delay_ms（100）;
```
------------------------------------------------

### 3.5.2. 秒延时等待
* 函数名称：robot.delay_sec(number s)
* 参数：  
    ms->等待秒数    
* 返回值：无
* 功能：秒延时等待
* 注意：无
* 例：
```lua
-- 等待1s
robot.delay_sec（1）;
```
------------------------------------------------

### 3.5.3. 设置参考坐标系
* 函数名称：robot.set_ref_frame(table ref)
* 参数：  
    ref->参考坐标系参数    
* 返回值：无
* 功能：设定参考坐标系
* 注意：对该函数后的任何位姿描述，均相对于该参考坐标系
* 例：
```lua
-- 设置参考坐标系
frame1 = {["x"] = 0, ["y"] = 0, ["z"] = 0,["rx"] = 0,["ry"] = 0,["rz"] = 0}
robot.set_ref_frame(frame1);
```
------------------------------------------------


### 3.5.4. 脚本例程
*　添加部分参考的脚本例程

### 3.5.5. 例程1
```lua
--选择机械臂平台
robot.set_robot_struct("scara_1","0xffa50001","0xffa60001")
--参考坐标系1和参考坐标系2
ref_frame1 = {["x"] = 0, ["y"] = 0, ["z"] = 0,["rx"] = 0,["ry"] = 0,["rz"] = 0}
ref_frame2 = {["x"] = 100, ["y"] = 1, ["z"] = 2,["rx"] = 99,["ry"] = 10,["rz"] = 5}

--设置参考坐标系为ref_frame1
robot.set_ref_frame(ref_frame1);
-- pose姿态：x,y,z为笛卡尔坐标位置，rx,ry,rz为rpy姿态（回转角、俯仰角、偏转角）
pose1 = {["x"] = 300, ["y"] = 0, ["z"] = 0,["rx"] = 0,["ry"] = 0,["rz"] = 0}
pose2 = {["x"] = 200, ["y"] = 0, ["z"] = 0,["rx"] = 0,["ry"] = 0,["rz"] = 0}
-- user_vel用户速度参数：j_vel,j_acc,j_dec,j_jerk分别为关节的目标速度、加速度、减速度、jerk(单位1，取值0.0001-1)
-- l_vel,l_acc,l_dec,l_jerk分别为曲线运动的线性速度参数(基本单位为：mm，取值0-100mm)
-- r_vel,r_acc,r_dec,r_jerk分别为rpy姿态旋转的转动速度(基本单位为：rad，取值0-0.5rad)
user_vel2 = {
    ["j_vel"] = 0.9, ["j_acc"] = 0.9, ["j_dec"] = 0.9, ["j_jerk"] = 0.9,
    ["l_vel"] = 100, ["l_acc"] = 100, ["l_dec"] = 0.9, ["l_jerk"] = 0.9,
    ["r_vel"] = 1.2, ["r_acc"] = 1.2, ["r_dec"] = 1.2, ["r_jerk"] = 1.2}
user_vel1 = {
    ["j_vel"] = 0.5, ["j_acc"] = 0.5, ["j_dec"] = 0.5, ["j_jerk"] = 0.5,
    ["l_vel"] = 100, ["l_acc"] = 100, ["l_dec"] = 0.9, ["l_jerk"] = 0.9,
    ["r_vel"] = 1.2, ["r_acc"] = 1.2, ["r_dec"] = 1.2, ["r_jerk"] = 1.2}
-- user_ovl用户逼近参数：rel为相对逼近参数（单位1，取值0-1），
user_ovl1 = {["rel"] = 0.5,["abs"] = 10,["rad"] = 0.1}
--添加点到点连续轨迹运动
robot.movep_cp_pose(pose1,0,user_vel1,user_ovl1,1)
robot.movep_cp_pose(pose2,0,user_vel1,user_ovl1,1)
robot.movep_cp_pose(pose1,0,user_vel2,user_ovl1,1)
-- 等待之前所有的运动结束，连续运动进入前瞻buff需要等待运动完成
robot.wait_move_finished()

--选择参考坐标系为ref_frame2
robot.set_ref_frame(ref_frame2)
--添加点到点单指令运动,单指令运动，没有运动逼近参数，且函数结束即运动完成或发生错误
robot.movep_sp_pose(pose2,0,user_vel1,1)


robot.report_error("here test error")
robot.report_warn("here test warn")
robot.report_info("here test info")

--打开扩展IO板数字输出通道1
robot.set_mio_digital(1,1)
--打开工具IO板数字输出通道1
robot.set_gio_digital(1,1)

--读取扩展IO数字输入通道1
a = robot.read_mio_digital(1)
print("mio1:",a)

--读取工具IO数字输入通道1
a = robot.read_gio_digital(1)
print("gio1:",a)

--设置扩展IO板DA输出，组别为第1组，输出模式为电流输出，输出数值为20.5mA
a = robot.set_mio_da(1,0,20.5);
print("mio_da:",a)

--设置工具IO板DA输出，组别为第1组，输出模式为电流输出，输出数值为20.5mA
a = robot.set_gio_da(1,0,20.5);
print("gio_da:",a)

--设置扩展IO板AD输入模式,电流采样模式，触发关闭，触发值为0
a = robot.set_mio_ad(1,0,0,0);
print("mio_s_ad:",a)

--设置工具IO板AD输入模式,电流采样模式，触发关闭，触发值为0
a = robot.set_gio_ad(1,0,0,0);
print("gio_s_ad:",a)

--读取扩展IO板AD采样值
a = robot.read_mio_ad(1);
print("gio_g_ad:",a)

--读取工具IO板AD采样值
a = robot.read_gio_ad(1);
print("gio_g_ad:",a)

--设置扩展IO板DOF输出，通道1输出模式为频率输出，输出100个脉冲信号
a = robot.set_mio_dof(1,1,100,0,1000);
print("mio_dof:",a)

--设置工具IO板DOF输出，通道1输出模式为频率输出，输出100个脉冲信号
a = robot.set_gio_dof(1,1,100,0,1000);
print("gio_dof:",a)

--设置扩展IO板DOV输出，通道1输出模式为12V输出
a = robot.set_mio_dov(1,1);
print("mio_dov:",a);

--设置工具IO板DOV输出，通道1输出模式为12V输出
a = robot.set_gio_dov(1,1);
print("gio_dov:",a);

for i=0,10,1
	do
	robot.delay_sec(1)
	print(i)
end

for i=0,1000,1
	do
	robot.delay_ms(10)
	print(i)
end
```
------------------------------------------------

## 3.6. 机械臂连接
* 每台机械臂都有32位的VIO(虚拟双向IO)，机械臂之前可以通过VIO进行通信

### 3.6.1. 设置VIO
* 函数名称：robot.set_vio(number context, number channel, number state)
* 参数：  
	- context: 0：标示机械臂本身，1-5标示其他的机械臂（该值可以通过robot.connect_machine函数获取）
    - channel: VIO的通道号，取值范围（1-32）
	- state: 输出状态，取值范围（0和1）
* 返回值：无
* 功能：设置VIO
* 注意：
* 例：
```lua
-- 设置机械臂本身的vio，通道1为1
robot.set_vio(0,1,1);
-- 创建192.168.1.61的链接context
context_61 = robot.connect_machine("192.168.1.61");
-- 设备context_61机械臂的vio，通道1为1
robot.set_vio(context_61,1,1);
```

### 3.6.2. 读取VIO
* 函数名称：robot.read_vio(number context, number channel)
* 参数：  
    - context: 0：标示机械臂本身，1-5标示其他的机械臂（该值可以通过robot.connect_machine函数获取）
    - channel: VIO的通道号，取值范围（1-32）
* 返回值：
	- 0,1：标示VIO状态
	- 其他值：读取失败
* 功能：读取VIO
* 注意：
* 例：
```lua
-- 读取自身的vio
a = robot.read_vio(0，1);
-- 创建192.168.1.61的链接context
context_61 = robot.connect_machine("192.168.1.61");
-- 读取 设备context_61机械臂的vio，通道1
a = robot.set_vio(context_61,1);
```

### 3.6.3. 创建和其他机械臂链接的context
* 函数名称：robot.connect_machine(string ip)
* 参数：  
    - ip: 其他机械臂的ip地址
* 返回值：
	- 1-5：链接成功，标示链接的context
	- 其他值：失败
* 功能：创建机械臂链接
* 注意：
* 例：
```lua
-- 创建192.168.1.61的链接context
context_61 = robot.connect_machine("192.168.1.61");
```

### 3.6.4. 断开和其他机械臂链接的context
* 函数名称：robot.disconnect_machine(number context)
* 参数：  
    - context: 机械臂链接的context
* 返回值：无
* 功能：断开机械臂链接
* 注意：
* 例：
```lua
-- 创建192.168.1.61的链接context
context_61 = robot.connect_machine("192.168.1.61");
-- 断开192.168.1.61链接
robot.disconnect_machine(context_61);
```


# 4. 对主控器API说明

## 4.1. 回调函数
此回调函数在lrobot.h中有声明，需要在使用时赋值。
```c
/*驱动器接口消息回调函数*/
typedef int lua_msg_callback(int reason,char *msg);

driver_interface_msg_callback  *msg_callback;
//使用示例：

static int32_t LUA_CALL_MSG(int32_t reason, char*msg)
{
	LUA_CALLBACK_PARAM *p=(LUA_CALLBACK_PARAM *)msg;
	switch(reason)
	{
	case LUA_MSG_SCRIPT_START:            //脚本启动
	{
		printf("start\n");
		break;
	}
	case LUA_MSG_SCRIPT_FINISHED:        		//脚本正常结束,流水脚本运行到结束
	{
		printf("finish\n");
		break;
	}
	case LUA_MSG_SERVO_ENABLE:           		//伺服使能
	{
		int *enable=p->u.enable_servo;
		printf("enable servo:%d,%d,%d,%d\n",enable[0],enable[1],enable[2],enable[3]);
		//启动错误 p->ret=-1;
		break;
	}
	case LUA_MSG_SERVO_STOP:              		//停止伺服
	{
		printf("stop servo\n");
		break;
	}
	case LUA_MSG_SET_ROBOT_TYPE:					//设置机械臂类型
	{
		LUA_ROBOT_TYPE *type;
		type=&p->u.lr_type;
		printf("robot type:%s,%s,%s\n",type->devname,type->mio_id,type->gio_id);
		//设置错误 p->ret=-1;,将导致脚本退出
		break;
	}
	case LUA_MSG_LOG_ERROR:						//日志错误
	{
		const char *rep_msg=p->u.rep_msg;
		printf("rep err:%s\n",rep_msg);
		break;
	}
	case LUA_MSG_LOG_WARN:						//日志警告
	{
		const char *rep_msg=p->u.rep_msg;
		printf("rep warn:%s\n",rep_msg);
		break;
	}
	case LUA_MSG_LOG_INFO: 						//日志报告
	{
		const char *rep_msg=p->u.rep_msg;
		printf("rep info:%s\n",rep_msg);
		break;
	}
	case LUA_MSG_MOVE_CP_POSE:			//点到点连续轨迹参数为pose运动 
	{
		LUA_ROBOT_MP_CP *cp=p->u.cp;
		printf("move cp:%d,%d,%lf,%lf,%lf,%lf,%lf\n",cp->enable,cp->ik_flag,cp->pose.x,cp->jvel.acca,cp->lvel.acca,cp->rvel.acca,cp->vol.abs);
		//设置错误 p->ret=-1;
		break;
	}
	case LUA_MSG_MOVE_SP_POSE:					//点到点单指令运动参数为pose运动
	{
		LUA_ROBOT_MP_SP *sp=p->u.sp;
		printf("move sp:%d,%d,%lf,%lf,%lf,%lf\n",sp->enable,sp->ik_flag,sp->pose.x,sp->jvel.acca,sp->lvel.acca,sp->rvel.acca);
		//设置错误 p->ret=-1;
		break;
	}
	case LUA_MSG_MOVE_CPT_POSE:					//点到点连续轨迹参数为posit运动
	{
		LUA_ROBOT_MP_CPT *cp=p->u.cpt;
		printf("move cpt:%d,%lf,%lf,%lf,%lf,%lf\n",cp->enable,cp->posit[0],cp->jvel.acca,cp->lvel.acca,cp->rvel.acca,cp->vol.abs);
		//设置错误 p->ret=-1;
		break;
	}
	case LUA_MSG_MOVE_SPT_POSE:					//点到点单指令参数为posit运动
	{
		LUA_ROBOT_MP_SPT *sp=p->u.spt;
		printf("move spt:%d,%lf,%lf,%lf,%lf\n",sp->enable,sp->posit[0],sp->jvel.acca,sp->lvel.acca,sp->rvel.acca);
		//设置错误 p->ret=-1;
		break;
	}
	case LUA_MSG_REF_FRAME_POSE:					//设置坐标系
	{
		printf("set ref\n");
		break;
	}
	case LUA_MSG_WAIT_MOVE_FINISHED:				//等待运动结束
	{
		int *ret=(int *)msg; 
		printf("move wait\n");
		*ret=0;
		//运动未完成 *ret=-1;,完成*ret=0
		break;
	}
	case LUA_MSG_IO_SET_MIO_DO:			//设置主IO板
	{
		LUA_ROBOT_IO_OUT *out=&p->u.iout;
		printf("MIO_DO:%d,%d\n",out->channel,out->val);
		break;
	}
	case LUA_MSG_IO_SET_GIO_DO:					//设置工具IO板
	{
		LUA_ROBOT_IO_OUT *out=&p->u.iout;
		printf("GIO_DO:%d,%d\n",out->channel,out->val);
		break;
	}
	case LUA_MSG_IO_RD_MIO_DI:					//读取主IO板
	{
		LUA_ROBOT_IO_IN *in=&p->u.ioin;
		*in->val=0;	//读取状态值到val
		printf("MIO_DI:%d,%d\n",in->channel,*in->val);
		break;
	}
	case LUA_MSG_IO_RD_GIO_DI:					//读取工具IO板	
	{
		LUA_ROBOT_IO_IN *in=&p->u.ioin;
		*in->val=1;	//读取状态值到val
		printf("GIO_DI:%d,%d\n",in->channel,*in->val);
		break;
	}
	case LUA_MSG_DA_SET_MIO:					//设置DA主IO板
	{
		STRUCT_DA *da=p->u.da;
		printf("MIO_DA:%d,%d,%f\n",da->channel,da->mode,da->data);
		break;
	}
	case LUA_MSG_DA_SET_GIO:					//设置DA工具IO板
	{
		STRUCT_DA *da=p->u.da;
		printf("GIO_DA:%d,%d,%f\n",da->channel,da->mode,da->data);
		break;
	}
	case LUA_MSG_AD_SET_MIO:					//设置AD主IO板
	{
		STRUCT_AD *ad=p->u.ad;
		printf("MIO_S_AD:%d,%d,%d,%d\n",ad->channel,ad->sample_mode,ad->trigger_mode,ad->trigger_data);
		break;
	}
	case LUA_MSG_AD_SET_GIO:					//设置AD工具IO板
	{
		STRUCT_AD *ad=p->u.ad;
		printf("GIO_S_AD:%d,%d,%d,%d\n",ad->channel,ad->sample_mode,ad->trigger_mode,ad->trigger_data);
		break;
	}
	case LUA_MSG_AD_RD_MIO:					//读取AD主IO板
	{
		LUA_ROBOT_IO_ADC *rad=&p->u.r_ad;
		*rad->val=0.55f;
		printf("MIO_R_AD:%d,%f\n",rad->channel,*rad->val);
		break;
	}
	case LUA_MSG_AD_RD_GIO:					//读取AD工具IO板
	{
		LUA_ROBOT_IO_ADC *rad=&p->u.r_ad;
		*rad->val=0.55f;
		printf("GIO_R_AD:%d,%f\n",rad->channel,*rad->val);
		break;
	}
	case LUA_MSG_DOF_SET_MIO:					//设置DOF主IO板
	{
		STRUCT_DOF *dof=p->u.dof;
		printf("MIO_DOF:%d,%f,%d,%d,%d\n",dof->channel,dof->frequency,dof->mode,dof->param2,dof->param3);
		break;
	}
	case LUA_MSG_DOF_SET_GIO:					//设置DOF工具IO板
	{
		STRUCT_DOF *dof=p->u.dof;
		printf("GIO_DOF:%d,%f,%d,%d,%d\n",dof->channel,dof->frequency,dof->mode,dof->param2,dof->param3);
		break;
	}
	case LUA_MSG_DOV_SET_MIO:					//设置DOV主IO板
	{
		STRUCT_DOV *dov=p->u.dov;
		printf("MIO_DOV:%d,%d\n",dov->channel,dov->mode);
		break;
	}
	case LUA_MSG_DOV_SET_GIO:					//设置DOV工具IO板
	{
		STRUCT_DOV *dov=p->u.dov;
		printf("GIO_DOV:%d,%d\n",dov->channel,dov->mode);
		break;
	}
	case LUA_WARN_STATE_NOT_PERMIT:		//当前状态不允许此操作
	{
		const char *inf=(const char *)msg;
		printf("warn_permit:%s\n",inf);
		break;
	}
	case LUA_WARN_SCRIPT_USER_STOP:				//脚本用户停止结束或调用robot.exit退出
	{
		printf("user stop\n");
		break;
	}
	case LUA_WARN_SCRIPT_RUN_ERR:				//运行脚本时出现语法或其他错误 
	{
		int line=(int)msg;
		printf("err stop,line=%d\n",line);
		break;
	}
	case LUA_ERR_FILENAME:				//传入lua脚本路径错误
	{
		printf("err file\n");
		break;
	}
	}
	return 0;
}

//回调函数需要根据消息类型的不同，接收不同类型的数据（需要将msg指针进行类型转换），来做一些其他操作。
```
## 4.2. 回调函数消息类型

```c
#ifndef __LRROBOT_INTERFACE_H__
#define __LRROBOT_INTERFACE_H__
#include "lr_def.h"

//消息回调
typedef enum
{
	//消息回调1000-1999
	LUA_MSG_SCRIPT_START = 1001,            //脚本启动
	LUA_MSG_SCRIPT_FINISHED,         		//脚本正常结束,流水脚本运行到结束
	LUA_MSG_SERVO_ENABLE,            		//伺服使能
	LUA_MSG_SERVO_STOP,              		//停止伺服
	LUA_MSG_SET_ROBOT_TYPE,					//设置机械臂类型
	
	//日志类错误码
	LUA_MSG_LOG_ERROR, 						//日志错误
	LUA_MSG_LOG_WARN, 						//日志警告
	LUA_MSG_LOG_INFO, 						//日志报告

	//点到点轨迹运动
	LUA_MSG_MOVE_CP_POSE = 1010,			//点到点连续轨迹参数为pose运动 
	LUA_MSG_MOVE_SP_POSE ,					//点到点单指令运动参数为pose运动
	LUA_MSG_MOVE_CPT_POSE,					//点到点连续轨迹参数为posit运动
	LUA_MSG_MOVE_SPT_POSE,					//点到点单指令参数为posit运动
	LUA_MSG_REF_FRAME_POSE,					//设置坐标系
	LUA_MSG_WAIT_MOVE_FINISHED,				//等待运动结束
	
	
	//IO控制
	LUA_MSG_IO_SET_MIO_DO = 1020,			//设置主IO板
	LUA_MSG_IO_SET_GIO_DO,					//设置工具IO板
	LUA_MSG_IO_RD_MIO_DI,					//读取主IO板
	LUA_MSG_IO_RD_GIO_DI,					//读取工具IO板	
	LUA_MSG_DA_SET_MIO ,					//设置DA主IO板
	LUA_MSG_DA_SET_GIO ,					//设置DA工具IO板
	LUA_MSG_AD_SET_MIO ,					//设置AD主IO板
	LUA_MSG_AD_SET_GIO ,					//设置AD工具IO板
	LUA_MSG_AD_RD_MIO  ,					//读取AD主IO板
	LUA_MSG_AD_RD_GIO  ,					//读取AD工具IO板
	LUA_MSG_DOF_SET_MIO,					//设置DOF主IO板
	LUA_MSG_DOF_SET_GIO,					//设置DOF工具IO板
	LUA_MSG_DOV_SET_MIO,					//设置DOV主IO板
	LUA_MSG_DOV_SET_GIO,					//设置DOV工具IO板

	//查询操作
	LUA_MSG_QUEUE_SERVO_DONE = 1050,		//查询伺服使能状态
	LUA_MSG_QUEUE_MOTION_PATH_BUFF_FULL,	//查询运动控制库缓存是否满，即是否允许写入
	LUA_MSG_QUEUE_MOTION_DONE,				//查询运动是否完成

	//警告消息
	LUA_WARN_STATE_NOT_PERMIT = 2001,		//当前状态不允许此操作
	LUA_WARN_SCRIPT_USER_STOP,				//脚本用户停止结束或调用robot.exit退出
	LUA_WARN_SCRIPT_RUN_ERR,				//运行脚本时出现语法或其他错误 

	//错误消息2000-2999
	LUA_ERR_FILENAME = 3001,				//传入lua脚本路径错误
}LUA_CALLBACK_REASON;

typedef struct
{
	const char *devname;
	const char *mio_id;
	const char *gio_id;
}LUA_ROBOT_TYPE;
typedef struct
{
	int channel;
	int val;
}LUA_ROBOT_IO_OUT;

typedef struct
{
	int channel;
	int *val;
}LUA_ROBOT_IO_IN;

typedef struct
{
	int channel;
	float *val;
}LUA_ROBOT_IO_ADC;

typedef struct
{
	int ret;
	union
	{
		LUA_ROBOT_TYPE lr_type;
		LUA_ROBOT_MP_CP *cp;
		LUA_ROBOT_MP_SP *sp;
		LUA_ROBOT_MP_CPT *cpt;
		LUA_ROBOT_MP_SPT *spt;
		int *enable_servo;
		const char *rep_msg;
		LUA_ROBOT_IO_OUT iout;
		LUA_ROBOT_IO_IN ioin;
		STRUCT_DA	*da;
		STRUCT_DOV 	*dov;
		STRUCT_AD	*ad;
		STRUCT_DOF	*dof;
		LUA_ROBOT_IO_ADC r_ad;
	}u;
}LUA_CALLBACK_PARAM;

//lua消息回调函数
typedef int32_t (*lua_callback_msg)(int32_t reason, char*msg);

typedef enum 
{
	LR_NOINIT,	//未初始化	
	LR_IDLE,	//空闲
	LR_LOADFILE,	//加载lua文件
	LR_RUNNING,		//运行脚本中
	LR_SUSPEND,		//暂停
	LR_RESUME,		//恢复
	LR_STOP_SET,	//设置停止
	LR_STOPED,		//已停止
	LR_ERR,			//脚本运行错误
	LR_EXITING,		//退出中
	LR_EXIT			//已退出,线程已完成,重新解析需要重新加载lrobot_init
}LR_STATE;
//********************************************************************
//* 函数名称: lrobot_init
//* 函数描述: robot类的接口初始化,创建一个线程
//* 输入参数: msg_callback 事件及消息回调函数
//* 输出参数: 
//* 返回说明: 
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_init(lua_callback_msg msg_callback);
//********************************************************************
//* 函数名称: lrobot_start
//* 函数描述: 启动lua脚本
//* 输入参数: lua_file,lua文件路径
//* 输出参数: 
//* 返回说明: 0:成功,-1:当前状态不允许,-2,文件名为空,-3,文件不存在
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_start(char *lua_file);
//********************************************************************
//* 函数名称: lrobot_pause
//* 函数描述: 暂停lua脚本
//* 输入参数: 
//* 输出参数:
//* 返回说明:  0,成功,-1,当前状态不允许(非运行状态)
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_pause(void);
//********************************************************************
//* 函数名称: lrobot_continue
//* 函数描述: 继续lua脚本
//* 输入参数: 
//* 输出参数: 
//* 返回说明: 0,成功,-1,当前状态不允许(非运行状态)
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_continue(void);
//********************************************************************
//* 函数名称: lrobot_stop
//* 函数描述: 停止lua脚本
//* 输入参数: 
//* 输出参数: 
//* 返回说明: 0,成功,-1,当前状态不允许(非运行状态或暂停状态)
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_stop(void);
//********************************************************************
//* 函数名称: lrobot_clearerr
//* 函数描述: 清除lua脚本运行错误(lua脚本语法错误或其他问题导致退出)
//* 输入参数: 
//* 输出参数: 
//* 返回说明: 0,成功,-1,当前状态不允许
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_clearerr(void);
//********************************************************************
//* 函数名称: lrobot_getstate
//* 函数描述: 获取lua脚本运行状态
//* 输入参数: 
//* 输出参数: 
//* 返回说明: 返回状态,查看LR_STATE定义
//* 其他:     18-4-2017, by tangb
//********************************************************************
LR_STATE lrobot_getstate(void);
//********************************************************************
//* 函数名称: lrobot_setbreakline
//* 函数描述: 设置调试断点
//* 输入参数: line 调试行数
//* 输出参数: 
//* 返回说明: 0,成功
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_setbreakline(int line);
//********************************************************************
//* 函数名称: lrobot_getrunline
//* 函数描述: 得到当前运行的行数
//* 输入参数: 
//* 输出参数: 
//* 返回说明: 返回行数
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_getrunline(void);
//********************************************************************
//* 函数名称: lrobot_exit
//* 函数描述: 退出lua脚本解析线程,恢复需要重新调用lrobot_init
//* 输入参数: 
//* 输出参数: 
//* 返回说明: 0,成功
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_exit(void);
#endif
```
