#Generated by VisualGDB (http://visualgdb.com)
#DO NOT EDIT THIS FILE MANUALLY UNLESS YOU ABSOLUTELY NEED TO
#USE VISUALGDB PROJECT PROPERTIES DIALOG INSTEAD
TARGETNAME := liblua_linux_arm.a
TARGETTYPE := STATIC
BINARYDIR := ./lib/arm

#Toolchain
CC := arm-linux-gnueabihf-gcc
CXX := arm-linux-gnueabihf-g++
LD := $(CXX)
AR := arm-linux-gnueabihf-ar
OBJCOPY := arm-linux-gnueabihf-objcopy

SOURCEFILES :=

#Additional flags
PREPROCESSOR_MACROS := DEBUG=1 linux
INCLUDE_DIRS := ./interface ./include
LIBRARY_DIRS := 
LIBRARY_NAMES := 
ADDITIONAL_LINKER_INPUTS := 
MACOS_FRAMEWORKS := 
LINUX_PACKAGES := 

CFLAGS := -ggdb -ffunction-sections -O3
CXXFLAGS := -ggdb -ffunction-sections -O3
ASFLAGS := 
LDFLAGS := -Wl,-gc-sections
COMMONFLAGS := 
LINKER_SCRIPT := 

START_GROUP := -Wl,--start-group
END_GROUP := -Wl,--end-group

#Additional options detected from testing the toolchain
IS_LINUX_PROJECT := 1
