#ifndef __EXT_DEF_H__
#define __EXT_DEF_H__

#include <stdint.h>

//#define _DEBUG_

#ifdef _DEBUG_
#define MAX_JOINT 9
typedef struct {
    float vel;          //目标速度
    float acca;         //加速段最大加速度
    float accd;         //减速段最大加速度
    float jerk;         //加加速度
} STRUCT_USER_VEL_BASE; //基础速度结构体

//机械臂位姿结构体
typedef struct {
    double x, y, z;    //位置
    double rx, ry, rz; //姿态rotx,roty,rotz
    double u, v, w;    //辅助轴姿态u,v,w
} STRUCT_POSE;

//用户运动逼近参数
typedef struct {
    float rel; //相对逼近参数(取值范围 0-1),用于点到点逼近
    float abs; //绝对位置逼近参数(取值范围 0-1000mm),用于曲线逼近---暂时无效
    float rad; //绝对姿态逼近参数(取值范围 0-1rad),用于姿态旋转逼近---暂时无效
} STRUCT_USER_OVL;

/*IO板功能结构体*/
typedef struct
{
    uint8_t channel; //通道号：0x00到0x07分别表示通道1到8
    uint8_t mode;    //DOV输出模式：0->关闭，1->12V，2->24V
} STRUCT_DOV;        //DOV 数字电压输出

typedef struct
{
    uint8_t channel;      //通道号：0x00到0x07分别表示通道1到8
    uint8_t sample_mode;  //采样模式：0->4~20mA电流模式，1->0~5V电压模式，2~>0~10V电压模式
    float sample_data;    //实际采样值，电流模式:4.0~20.0, 0~5V电压模式:0~5.0, 0~10V电压模式：0~10.0
    uint8_t trigger_mode; //触发模式：0->关闭 1->大于等于 2->小于等于
    float trigger_data;   //实际触发值，电流模式:4.0~20.0, 0~5V电压模式:0~5.0, 0~10V电压模式：0~10.0
} STRUCT_AD;              //AD采集结构体

typedef struct
{
    uint8_t channel; //通道号：0x00到0x07分别表示通道1到8
    uint8_t mode;    //DA输出模式： 0->电流输出 1->电压输出
    float data;      //DA数据：电流输出模式：4.0~20.0。电压输出模式：0~10.0
} STRUCT_DA;         //DA输出结构体

typedef struct
{
    uint8_t channel; //通道号：0x00到0x07分别表示通道1到8
    uint8_t mode;    //DOF输出模式：0->普通IO模式 1->频率输出 2->PWM输出
    uint16_t param2; //参数2：在普通IO模式无效；在频率输出模式表示脉冲个数；在PWM输出模式表示占空比(1~99)；
    uint8_t param3;  //参数3：普通IO模式 1->打开，0->关闭,其他模式,表示初始IO输出状态(输入奇数->1,输入偶数->0)
    float frequency; //频率：0.1~1000.0Hz.普通IO模式无效
} STRUCT_DOF;        //DOF数字功能输出结构体

#else
#include "motion9_dll.h"
#include "qx_ctrl_io.h"

#endif

/* 点到点轨迹 */
typedef struct movep_cp_pose_info { //msg=io_digital_info
    int enable;                     //有效使能
    int ik_flag;                    //运动学逆解姿态 0：右臂 1：左臂
    STRUCT_POSE pose;               //位姿参数
    STRUCT_USER_VEL_BASE jvel;      //用户速度参数
    STRUCT_USER_VEL_BASE lvel;
    STRUCT_USER_VEL_BASE rvel;
    STRUCT_USER_OVL vol; //用户逼近参数
} LUA_ROBOT_MP_CP;

typedef struct movep_sp_pose_info {
    int enable;                //有效使能
    int ik_flag;               //运动学逆解姿态 0：右臂 1：左臂
    STRUCT_POSE pose;          //位姿参数
    STRUCT_USER_VEL_BASE jvel; //用户速度参数
    STRUCT_USER_VEL_BASE lvel;
    STRUCT_USER_VEL_BASE rvel;
} LUA_ROBOT_MP_SP;

typedef struct movep_cp_posit_info {
    int enable;                //有效使能
    double posit[MAX_JOINT];   //关节位置参数
    STRUCT_USER_VEL_BASE jvel; //用户速度参数
    STRUCT_USER_VEL_BASE lvel;
    STRUCT_USER_VEL_BASE rvel;
    STRUCT_USER_OVL vol; //用户逼近参数
} LUA_ROBOT_MP_CPT;

typedef struct movep_sp_posit_info {
    int enable;                //有效使能
    double posit[MAX_JOINT];   //关节位置参数
    STRUCT_USER_VEL_BASE jvel; //用户速度参数
    STRUCT_USER_VEL_BASE lvel;
    STRUCT_USER_VEL_BASE rvel;
} LUA_ROBOT_MP_SPT;

typedef struct
{
    uint8_t modelSeq;  //模型序号
    float m_core_x;    //模型重心x轴坐标
    float m_core_y;    //模型重心y轴坐标
    float rotateAngle; //模型旋转角度
    float model_x[3];  //模型的x坐标
    float model_y[3];  //模型的y坐标
} STRUCT_MODEL_INFO;

#endif
