/********************************************************************
**   文 件 名:	lrobot.c
**   Copyright   (c)   2017  杭州琦星机器人科技有限公司
**   创 建 人:	tangb 
**   日    期:	2017.4.18
**   修 改 人:	
**   功能描述:	
**   版    本:  v1.01
********************************************************************/
#ifdef linux
#include <unistd.h>
#else
#include <windows.h>
#endif

#include "frame_reference.h"
#include "lrobot.h"
#include <time.h>

#define RETURN_VAL(l, v)       \
    {                          \
        lua_pushinteger(l, v); \
        return 1;              \
    }

#define RETURN_VAL_F(l, v)    \
    {                         \
        lua_pushnumber(l, v); \
        return 1;             \
    }

#define CHECK_ARGS(l, n)        \
    if (lua_gettop(l) != n) {   \
        lua_pushinteger(l, -1); \
        return 1;               \
    }

#define GET_TAB_KEYVAL(l, idx, key, val) \
    lua_getfield(l, idx, key);           \
    val = lua_tonumber(l, lua_gettop(l));

static LUA_ROBOT_MP_CP _mp_cp;
static LUA_ROBOT_MP_SP _mp_sp;
static LUA_ROBOT_MP_CPT _mp_cpt;
static LUA_ROBOT_MP_SPT _mp_spt;
static STRUCT_POSE _pose_ref_param;

static DO_EVENT do_event;
static STRUCT_USER_VEL_BASE *get_vel_param(lua_State *L, STRUCT_USER_VEL_BASE *vel, int idx, char type) {
    const char *key_t[3][4] = {{"j_vel", "j_acc", "j_dec", "j_jerk"},
                               {"l_vel", "l_acc", "l_dec", "l_jerk"},
                               {"r_vel", "r_acc", "r_dec", "r_jerk"}};
    const char **key;
    if ((type < 0) || (type > 3)) return NULL;
    key = &key_t[type][0];
    GET_TAB_KEYVAL(L, idx, key[0], vel->vel);
    GET_TAB_KEYVAL(L, idx, key[1], vel->acca);
    GET_TAB_KEYVAL(L, idx, key[2], vel->accd);
    GET_TAB_KEYVAL(L, idx, key[3], vel->jerk);
    return vel;
}

static STRUCT_POSE *get_pos_param(lua_State *L, STRUCT_POSE *pos, int idx) {
    const char *key[6] = {"x", "y", "z", "rx", "ry", "rz"};
    STRUCT_POSE pos_t;
    GET_TAB_KEYVAL(L, idx, key[0], pos_t.x);
    GET_TAB_KEYVAL(L, idx, key[1], pos_t.y);
    GET_TAB_KEYVAL(L, idx, key[2], pos_t.z);
    GET_TAB_KEYVAL(L, idx, key[3], pos_t.rx);
    GET_TAB_KEYVAL(L, idx, key[4], pos_t.ry);
    GET_TAB_KEYVAL(L, idx, key[5], pos_t.rz);
    frComputePose(&_pose_ref_param, &pos_t, pos);
    return pos;
}

static STRUCT_POSE *set_pos_ref_param(lua_State *L, int idx) {
    const char *key[6] = {"x", "y", "z", "rx", "ry", "rz"};
    GET_TAB_KEYVAL(L, idx, key[0], _pose_ref_param.x);
    GET_TAB_KEYVAL(L, idx, key[1], _pose_ref_param.y);
    GET_TAB_KEYVAL(L, idx, key[2], _pose_ref_param.z);
    GET_TAB_KEYVAL(L, idx, key[3], _pose_ref_param.rx);
    GET_TAB_KEYVAL(L, idx, key[4], _pose_ref_param.ry);
    GET_TAB_KEYVAL(L, idx, key[5], _pose_ref_param.rz);
    return &_pose_ref_param;
}

static double *get_post_param(lua_State *L, double *post, int idx) {
    const char *key[6] = {"j1", "j2", "j3", "j4", "j5", "j6"};
    GET_TAB_KEYVAL(L, idx, key[0], post[0]);
    GET_TAB_KEYVAL(L, idx, key[1], post[1]);
    GET_TAB_KEYVAL(L, idx, key[2], post[2]);
    GET_TAB_KEYVAL(L, idx, key[3], post[3]);
    GET_TAB_KEYVAL(L, idx, key[4], post[4]);
    GET_TAB_KEYVAL(L, idx, key[5], post[5]);
    return post;
}

static STRUCT_USER_OVL *get_vol_param(lua_State *L, STRUCT_USER_OVL *vol, int idx) {
    const char *key[3] = {"rel", "abs", "rad"};
    GET_TAB_KEYVAL(L, idx, key[0], vol->rel);
    GET_TAB_KEYVAL(L, idx, key[1], vol->abs);
    GET_TAB_KEYVAL(L, idx, key[2], vol->rad);
    return vol;
}

//********************************************************************
//* 函数名称: servo_enable
//* 函数描述: 打开使能伺服
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int servo_enable(lua_State *L) {
    int j[MAX_JOINT], i, ret;
    char key[4] = "j1";
    CHECK_ARGS(L, 1);
    if (lua_istable(L, -1)) RETURN_VAL(L, -1);
    for (i = 0; i < MAX_JOINT; i++) {
        key[1] = '1' + i;
        GET_TAB_KEYVAL(L, 1, key, j[i]); //堆栈地址的问题
    }
    ret = do_event(SERVO_ENABLE, j);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: servo_stop
//* 函数描述: 关闭使能伺服
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int servo_stop(lua_State *L) {
    int ret = do_event(SERVO_STOP);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: servo_exit
//* 函数描述: 退出脚本
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int servo_exit(lua_State *L) {
    int ret = do_event(ROBOT_EXIT);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称:
//* 函数描述: 设置机械臂类型
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
/*设置机械臂类型*/
static int set_robot_struct(lua_State *L) {
    int ret;
    const char *str;
    const char *mio;
    const char *gio;
    CHECK_ARGS(L, 3);
    str = lua_tostring(L, 1);
    mio = lua_tostring(L, 2);
    gio = lua_tostring(L, 3);
    ret = do_event(ROBOT_TYPE, str, mio, gio);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: report_error
//* 函数描述: 日志错误报告
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int report_error(lua_State *L) {
    int ret;
    const char *str;
    CHECK_ARGS(L, 1);
    str = lua_tostring(L, 1);
    ret = do_event(REP_ERR, str);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: report_warn
//* 函数描述: 日志警告报告
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int report_warn(lua_State *L) {
    int ret;
    const char *str;
    CHECK_ARGS(L, 1);
    str = lua_tostring(L, 1);
    ret = do_event(REP_WARN, str);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: report_info
//* 函数描述: 日志信息报告
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int report_info(lua_State *L) {
    int ret;
    const char *str;
    CHECK_ARGS(L, 1);
    str = lua_tostring(L, 1);
    ret = do_event(REP_INFO, str);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: movep_cp_pose
//* 函数描述: 点到点连续轨迹参数为pose运动
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int movep_cp_pose(lua_State *L) {
    int ret;
    CHECK_ARGS(L, 5);
    get_pos_param(L, &_mp_cp.pose, 1);
    get_vel_param(L, &_mp_cp.jvel, 3, 0);
    get_vel_param(L, &_mp_cp.lvel, 3, 1);
    get_vel_param(L, &_mp_cp.rvel, 3, 2);
    get_vol_param(L, &_mp_cp.vol, 4);
    _mp_cp.ik_flag = lua_tonumber(L, 2);
    _mp_cp.enable = lua_tonumber(L, 5);
    ret = do_event(MP_CP_POSE, &_mp_cp);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: movep_sp_pose
//* 函数描述: 点到点单指令运动参数为pose运动
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int movep_sp_pose(lua_State *L) {
    int ret;
    CHECK_ARGS(L, 4);
    get_pos_param(L, &_mp_sp.pose, 1);
    get_vel_param(L, &_mp_sp.jvel, 3, 0);
    get_vel_param(L, &_mp_sp.lvel, 3, 1);
    get_vel_param(L, &_mp_sp.rvel, 3, 2);
    _mp_sp.ik_flag = lua_tonumber(L, 2);
    _mp_sp.enable = lua_tonumber(L, 4);
    ret = do_event(MP_SP_POSE, &_mp_sp);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: movep_cp_posit
//* 函数描述: 点到点连续轨迹参数为posit运动
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int movep_cp_posit(lua_State *L) {
    int ret;
    CHECK_ARGS(L, 4);
    get_post_param(L, _mp_cpt.posit, 1);
    get_vel_param(L, &_mp_cpt.jvel, 2, 0);
    get_vel_param(L, &_mp_cpt.lvel, 2, 1);
    get_vel_param(L, &_mp_cpt.rvel, 2, 2);
    get_vol_param(L, &_mp_cpt.vol, 3);
    _mp_cpt.enable = lua_tonumber(L, 4);
    ret = do_event(MP_CPT_POSE, &_mp_cpt);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: movep_sp_posit
//* 函数描述: 点到点单指令参数为posit运动
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int movep_sp_posit(lua_State *L) {

    int ret;
    CHECK_ARGS(L, 4);
    get_post_param(L, _mp_spt.posit, 1);
    get_vel_param(L, &_mp_spt.jvel, 2, 0);
    get_vel_param(L, &_mp_spt.lvel, 2, 1);
    get_vel_param(L, &_mp_spt.rvel, 2, 2);
    _mp_cpt.enable = lua_tonumber(L, 3);
    ret = do_event(MP_SPT_POSE, &_mp_spt);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: wait_move_finished
//* 函数描述: 等待运动结束
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int wait_move_finished(lua_State *L) {
    int ret = do_event(MP_WAIT);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: set_mio_digital
//* 函数描述: 设置数字IO输出
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int set_mio_digital(lua_State *L) {
    int channel, state;
    int ret;
    CHECK_ARGS(L, 2);
    channel = lua_tonumber(L, 1);
    state = lua_tonumber(L, 2);
    ret = do_event(MIO_DOUT, channel, state);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: set_gio_digital
//* 函数描述: 设置数字IO输出
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int set_gio_digital(lua_State *L) {
    int channel, state;
    int ret;
    CHECK_ARGS(L, 2);
    channel = lua_tonumber(L, 2);
    state = lua_tonumber(L, 3);
    ret = do_event(GIO_DOUT, channel, state);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: read_mio_digital
//* 函数描述: 读取数字IO输入
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int read_mio_digital(lua_State *L) {
    int channel, state;
    int ret;
    CHECK_ARGS(L, 1);
    channel = lua_tonumber(L, 1);
    ret = do_event(MIO_DIN, channel, &state);
    if (ret < 0) RETURN_VAL(L, ret);
    RETURN_VAL(L, state);
}

//********************************************************************
//* 函数名称: read_gio_digital
//* 函数描述: 读取数字IO输入
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int read_gio_digital(lua_State *L) {
    int channel, state;
    int ret;
    CHECK_ARGS(L, 1);
    channel = lua_tonumber(L, 1);
    ret = do_event(GIO_DIN, channel, &state);
    if (ret < 0) RETURN_VAL(L, ret);
    RETURN_VAL(L, state);
}

//********************************************************************
//* 函数名称: set_mio_dac
//* 函数描述: 设置DA输出
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int set_mio_dac(lua_State *L) {
    STRUCT_DA da;
    int ret;
    CHECK_ARGS(L, 3);
    da.channel = lua_tonumber(L, 1);
    da.mode = lua_tonumber(L, 2);
    da.data = lua_tonumber(L, 3);
    ret = do_event(MIO_DAC, &da);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: set_gio_dac
//* 函数描述: 设置DA输出
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int set_gio_dac(lua_State *L) {
    STRUCT_DA da;
    int ret;
    CHECK_ARGS(L, 3);
    da.channel = lua_tonumber(L, 1);
    da.mode = lua_tonumber(L, 2);
    da.data = lua_tonumber(L, 3);
    ret = do_event(GIO_DAC, &da);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: set_mio_dov
//* 函数描述: 设置数字电压输出
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int set_mio_dov(lua_State *L) {
    STRUCT_DOV dov;
    int ret;
    CHECK_ARGS(L, 2);
    dov.channel = lua_tonumber(L, 1);
    dov.mode = lua_tonumber(L, 2);
    ret = do_event(MIO_DOV, &dov);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: set_gio_dov
//* 函数描述: 设置数字电压输出
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int set_gio_dov(lua_State *L) {
    STRUCT_DOV dov;
    int ret;
    CHECK_ARGS(L, 2);
    dov.channel = lua_tonumber(L, 1);
    dov.mode = lua_tonumber(L, 2);
    ret = do_event(GIO_DOV, &dov);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: set_mio_adc
//* 函数描述: 设置AD输入模式
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int set_mio_adc(lua_State *L) {
    STRUCT_AD ad;
    int ret;
    CHECK_ARGS(L, 4);
    ad.channel = lua_tonumber(L, 1);
    ad.sample_mode = lua_tonumber(L, 2);
    ad.trigger_mode = lua_tonumber(L, 3);
    ad.trigger_data = lua_tonumber(L, 4);
    ret = do_event(MIO_S_ADC, &ad);
    if (ret < 0) RETURN_VAL(L, -1); //如果设置不成功返回<0
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: set_gio_adc
//* 函数描述: 设置AD输入模式
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int set_gio_adc(lua_State *L) {
    STRUCT_AD ad;
    int ret;
    CHECK_ARGS(L, 4);
    ad.channel = lua_tonumber(L, 1);
    ad.sample_mode = lua_tonumber(L, 2);
    ad.trigger_mode = lua_tonumber(L, 3);
    ad.trigger_data = lua_tonumber(L, 4);
    ret = do_event(GIO_S_ADC, &ad);
    if (ret < 0) RETURN_VAL(L, -1); //如果设置不成功返回<0
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: read_mio_adc
//* 函数描述: 读取AD输入
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int read_mio_adc(lua_State *L) {
    int channel;
    float val;
    int ret;
    CHECK_ARGS(L, 1);
    channel = lua_tonumber(L, 1);
    ret = do_event(MIO_G_ADC, channel, &val);
    if (ret < 0) RETURN_VAL(L, -1); //如果设置不成功返回<0
    RETURN_VAL_F(L, val);
}

//********************************************************************
//* 函数名称: read_gio_adc
//* 函数描述: 读取AD输入
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int read_gio_adc(lua_State *L) {
    int channel;
    float val;
    int ret;
    CHECK_ARGS(L, 1);
    channel = lua_tonumber(L, 1);
    ret = do_event(GIO_G_ADC, channel, &val);
    if (ret < 0) RETURN_VAL(L, -1); //如果设置不成功返回<0
    RETURN_VAL_F(L, val);
}

//********************************************************************
//* 函数名称: set_mio_dof
//* 函数描述: 设置DOF输出
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int set_mio_dof(lua_State *L) {
    STRUCT_DOF dof;
    int ret;
    CHECK_ARGS(L, 5);
    dof.channel = lua_tonumber(L, 1);
    dof.mode = lua_tonumber(L, 2);
    dof.param2 = lua_tonumber(L, 3);
    dof.param3 = lua_tonumber(L, 4);
    dof.frequency = lua_tonumber(L, 5);
    ret = do_event(MIO_DOF, &dof);
    if (ret < 0) RETURN_VAL(L, -1); //如果设置不成功返回<0
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: set_gio_dof
//* 函数描述: 设置DOF输出
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int set_gio_dof(lua_State *L) {
    STRUCT_DOF dof;
    int ret;
    CHECK_ARGS(L, 5);
    dof.channel = lua_tonumber(L, 1);
    dof.mode = lua_tonumber(L, 2);
    dof.param2 = lua_tonumber(L, 3);
    dof.param3 = lua_tonumber(L, 4);
    dof.frequency = lua_tonumber(L, 5);
    ret = do_event(GIO_DOF, &dof);
    if (ret < 0) RETURN_VAL(L, -1); //如果设置不成功返回<0
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: read_model_value
//* 函数描述: 读取模型序号
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by yangg
//********************************************************************
static int read_model_value(lua_State *L) {
    STRUCT_MODEL_INFO model;
    int ret;
    CHECK_ARGS(L, 1);
    model.modelSeq = lua_tonumber(L, 1);
    ret = do_event(MODEL_INDEX, &model);
    if (ret < 0) RETURN_VAL(L, ret);
    lua_pushinteger(L, model.modelSeq);
    lua_pushnumber(L, model.m_core_x);
    lua_pushnumber(L, model.m_core_y);
    lua_pushnumber(L, model.rotateAngle);
    lua_pushnumber(L, model.model_x[0]);
    lua_pushnumber(L, model.model_y[0]);
    lua_pushnumber(L, model.model_x[1]);
    lua_pushnumber(L, model.model_y[1]);
    lua_pushnumber(L, model.model_x[2]);
    lua_pushnumber(L, model.model_y[2]);
    return 10;
}

//********************************************************************
//* 函数名称: set_mio_digital
//* 函数描述: 设置数字IO输出
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int set_server_ip(lua_State *L) {
    int ret;
    const char *str_ip;
    CHECK_ARGS(L, 1);
    str_ip = lua_tostring(L, 1);
    ret = do_event(SERVER_IP, str_ip);
    lua_pushstring(L, str_ip);
    return 1;
}

//********************************************************************
//* 函数名称: close_modbus_conn
//* 函数描述: 读取模型序号
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by yangg
//********************************************************************
/*static int close_modbus_conn(lua_State *L)
{
}*/

//********************************************************************
//* 函数名称: delay_msecond
//* 函数描述: 毫秒延时等待
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int delay_msecond(lua_State *L) {
    int ret, count, i;
    time_t start_time, now_time;
    double diff_time = 0, aim_time;
    CHECK_ARGS(L, 1);
    count = lua_tonumber(L, 1);
    aim_time = count / 1000;
    time(&start_time);
    while (diff_time < aim_time) {
#ifdef linux
        usleep(5000);
#else
        Sleep(5);
#endif
        time(&now_time);
        diff_time = difftime(now_time, start_time);
        ret = do_event(DLY_MS);
        if (ret < 0) break;
    }
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: delay_second
//* 函数描述: 秒延时等待
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int delay_second(lua_State *L) {
    int ret, count, i;
    time_t start_time, now_time;
    double diff_time = 0, aim_time;
    CHECK_ARGS(L, 1);
    count = lua_tonumber(L, 1);
    aim_time = count;
    time(&start_time);
    while (diff_time < aim_time) {
#ifdef linux
        usleep(5000);
#else
        Sleep(5);
#endif
        time(&now_time);
        diff_time = difftime(now_time, start_time);
        ret = do_event(DLY_SEC);
        if (ret < 0) break;
    }
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: set_ref_frame
//* 函数描述: 设置参考坐标系
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int set_ref_frame(lua_State *L) {
    int ret;
    CHECK_ARGS(L, 1);
    set_pos_ref_param(L, 1);
    ret = do_event(MP_REF_POSE);
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: connect_machine
//* 函数描述: 机械臂通信连接建立
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     2017-7-20, by qs
//********************************************************************
static int connect_machine(lua_State *L) {
    int ret;
    int context = -1;
    const char *dest_ip;
    CHECK_ARGS(L, 1);
    dest_ip = lua_tostring(L, 1);
    ret = do_event(CONNECT_MACHINE, &context, dest_ip);
    if (ret < 0) RETURN_VAL(L, -1); //如果设置不成功返回<0
    RETURN_VAL(L, context);
}

//********************************************************************
//* 函数名称: disconnect_machine
//* 函数描述: 机械臂通信断连接
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     2017-7-20, by qs
//********************************************************************
static int disconnect_machine(lua_State *L) {
    int ret;
    int context;
    CHECK_ARGS(L, 1);
    context = lua_tonumber(L, 1);
    ret = do_event(DISCONNECT_MACHINE, &context);
    if (ret < 0) RETURN_VAL(L, -1); //如果设置不成功返回<0
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: set_vio
//* 函数描述: 设置虚拟IO
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     2017-7-20, by qs
//********************************************************************
static int set_vio(lua_State *L) {
    int ret;
    int context, channel, val;
    CHECK_ARGS(L, 3);
    context = lua_tonumber(L, 1);
    channel = lua_tonumber(L, 2);
    val = lua_tonumber(L, 3);
    ret = do_event(SET_VIO, &context, channel, val);
    if (ret < 0) RETURN_VAL(L, -1); //如果设置不成功返回<0
    RETURN_VAL(L, ret);
}

//********************************************************************
//* 函数名称: read_vio
//* 函数描述: 读取虚拟io
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     2017-7-20, by qs
//********************************************************************
static int read_vio(lua_State *L) {
    int ret;
    int context, channel, val;
    CHECK_ARGS(L, 2);
    context = lua_tonumber(L, 1);
    channel = lua_tonumber(L, 2);
    ret = do_event(READ_VIO, &context, channel, &val);
    if (ret < 0) RETURN_VAL(L, -1); //如果设置不成功返回<0
    RETURN_VAL(L, val);
}

static const luaL_Reg robotlib[] = {
    {"servo_enable", servo_enable},
    {"servo_stop", servo_stop},
    {"exit", servo_exit},
    {"set_robot_struct", set_robot_struct},
    {"report_error", report_error},
    {"report_warn", report_warn},
    {"report_info", report_info},
    {"movep_cp_pose", movep_cp_pose},
    {"movep_sp_pose", movep_sp_pose},
    {"movep_cp_posit", movep_cp_posit},
    {"movep_sp_posit", movep_sp_posit},
    {"set_mio_digital", set_mio_digital},
    {"set_gio_digital", set_gio_digital},
    {"read_mio_digital", read_mio_digital},
    {"read_gio_digital", read_gio_digital},
    {"wait_move_finished", wait_move_finished},
    {"set_mio_dov", set_mio_dov},
    {"set_gio_dov", set_gio_dov},
    {"set_mio_da", set_mio_dac},
    {"set_gio_da", set_gio_dac},
    {"set_mio_ad", set_mio_adc},
    {"set_gio_ad", set_gio_adc},
    {"read_mio_ad", read_mio_adc},
    {"read_gio_ad", read_gio_adc},
    {"set_mio_dof", set_mio_dof},
    {"set_gio_dof", set_gio_dof},
    {"delay_ms", delay_msecond},
    {"delay_sec", delay_second},
    {"set_ref_frame", set_ref_frame},
    {"read_model_value", read_model_value},
    {"set_server_ip", set_server_ip},
    {"connect_machine", connect_machine},
    {"disconnect_machine", disconnect_machine},
    {"set_vio", set_vio},
    {"read_vio", read_vio},
    {NULL, NULL}};

LUAMOD_API int luaopen_robotlib(lua_State *L) {
    luaL_newlib(L, robotlib);
    return 1;
}

//********************************************************************
//* 函数名称: luaopen_robot
//* 函数描述: 注册lua脚本的robot类
//* 输入参数: do_event_call 事件回调函数指针
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
LUAMOD_API int luaopen_robot(lua_State *L, DO_EVENT do_event_call) {
    do_event = do_event_call;
    luaL_requiref(L, LUA_ROBOTLIBNAME, luaopen_robotlib, 1);
    lua_pop(L, 1);
    return 1;
}
