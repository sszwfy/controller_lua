#ifndef lrobot_h
#define lrobot_h

#include "lauxlib.h"
#include "lr_def.h"
#include "lua.h"
#include "lualib.h"
#include <stdint.h>

typedef enum {
    SERVO_ENABLE,
    SERVO_STOP,
    ROBOT_EXIT,
    ROBOT_TYPE,
    REP_ERR,
    REP_WARN,
    REP_INFO,
    MP_CP_POSE,
    MP_SP_POSE,
    MP_CPT_POSE,
    MP_SPT_POSE,
    MP_WAIT,
    MP_REF_POSE,
    MIO_DOUT,
    GIO_DOUT,
    MIO_DIN,
    GIO_DIN,
    MIO_DOV,
    GIO_DOV,
    MIO_DAC,
    GIO_DAC,
    MIO_S_ADC,
    GIO_S_ADC,
    MIO_G_ADC,
    GIO_G_ADC,
    MIO_DOF,
    GIO_DOF,
    DLY_MS,
    DLY_SEC,
    LOG_OUT,
    MODEL_INDEX,
    SERVER_IP,
    CONNECT_MACHINE,
    DISCONNECT_MACHINE,
    SET_VIO,
    READ_VIO
} LROBOT_EVENT_ID;
#define LUA_ROBOTLIBNAME "robot"
typedef int (*DO_EVENT)(LROBOT_EVENT_ID id, ...);
LUAMOD_API int luaopen_robot(lua_State *L, DO_EVENT do_event_call);
#endif /*lrobot_h*/
