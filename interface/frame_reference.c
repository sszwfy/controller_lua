#include <math.h>
#include "lr_def.h"
#include "frame_reference.h"

#define EPS_RPY (double) 1e-8
#define EPS_UELER (double) 1e-8

//static function
static void ClampT2HandT(STRUCT_POSE *frame_ref, double ct[12], double ht[12]);
static int KineMath_RPY2T(double T[12], double rpy[3], char flag);
static int KineMath_T2RPY(double T[12], double rpy[3], char flag);

//************************************
// Method:    KineMath_RPY2T
// FullName:  KineMath_RPY2T
// Access:    public 
// Returns:   int
// Qualifier:
// Parameter: double T[12]
// Parameter: double rpy[3]
// Parameter: char flag
//************************************
int KineMath_RPY2T(double T[12], double rpy[3], char flag)
{
	double cr, cp, cy, sr, sp, sy;
	cr = cos(rpy[0]);
	sr = sin(rpy[0]);
	cp = cos(rpy[1]);
	sp = sin(rpy[1]);
	cy = cos(rpy[2]);
	sy = sin(rpy[2]);
	if (!flag){
		//xyz
		T[0] = cp*cy;
		T[1] = -cp*sy;
		T[2] = sp;
		T[4] = cr*sy + cy*sp*sr;
		T[5] = cr*cy - sp*sr*sy;
		T[6] = -cp*sr;
		T[8] = sr*sy - cr*cy*sp;
		T[9] = cy*sr + cr*sp*sy;
		T[10] = cp*cr;
	}
	else{
		//zyx
		T[0] = cp*cr;
		T[1] = cr*sp*sy - cy*sr;
		T[2] = sr*sy + cr*cy*sp;
		T[4] = cp*sr;
		T[5] = cr*cy + sp*sr*sy;
		T[6] = cy*sp*sr - cr*sy;
		T[8] = -sp;
		T[9] = cp*sy;
		T[10] = cp*cy;
	}
	return 0;
}


//************************************
// Method:    KineMath_T2RPY
// FullName:  KineMath_T2RPY
// Access:    public 
// Returns:   int
// Qualifier:
// Parameter: double T[12]
// Parameter: double rpy[3]
// Parameter: char flag
//************************************
int KineMath_T2RPY(double T[12], double rpy[3], char flag)
{
	if (!flag){
		//xyz
		if (fabs(T[10]) < EPS_RPY && fabs(T[6]) < EPS_RPY){
			rpy[0] = 0;
			rpy[1] = atan2(T[2], T[10]);
			rpy[2] = atan2(T[4], T[5]);
		}
		else{
			double sr, cr;
			rpy[0] = atan2(-T[6], T[10]);
			sr = sin(rpy[0]);
			cr = cos(rpy[0]);
			rpy[1] = atan2(T[2], cr * T[10] - sr * T[6]);
			rpy[2] = atan2(-T[1], T[0]);
		}
	}
	else{
		//zyx
		if (fabs(T[0]) < EPS_RPY && fabs(T[4]) < EPS_RPY){
			rpy[0] = 0;
			rpy[1] = atan2(-T[8], T[0]);
			rpy[2] = atan2(-T[6], T[5]);
		}
		else{
			double sp, cp;
			rpy[0] = atan2(T[4], T[0]);
			sp = sin(rpy[0]);
			cp = cos(rpy[0]);
			rpy[1] = atan2(-T[8], cp * T[0] + sp * T[4]);
			rpy[2] = atan2(sp * T[2] - cp * T[6], cp*T[5] - sp*T[1]);
		}
	}
	return 0;
}

void ClampT2HandT(STRUCT_POSE *reference, double ct[12], double ht[12])
{
	double x = reference->x;
	double y = reference->y;
	double z = reference->z;
	double rx = reference->rx;
	double ry = reference->ry;
	double rz = reference->rz;
	double srx = sin(rx);
	double srz = sin(rz);
	double crx = cos(rx);
	double crz = cos(rz);
	double sry = sin(ry);
	double cry = cos(ry);
	ht[0] = crz*(ct[0] * cry + sry*(ct[2] * crx + ct[1] * srx)) - srz*(ct[1] * crx - ct[2] * srx);
	ht[1] = srz*(ct[0] * cry + sry*(ct[2] * crx + ct[1] * srx)) + crz*(ct[1] * crx - ct[2] * srx);
	ht[2] = cry*(ct[2] * crx + ct[1] * srx) - ct[0] * sry;
	ht[3] = ct[3] - x*(crz*(ct[0] * cry + sry*(ct[2] * crx + ct[1] * srx)) - srz*(ct[1] * crx - ct[2] * srx))
		- y*(srz*(ct[0] * cry + sry*(ct[2] * crx + ct[1] * srx)) + crz*(ct[1] * crx - ct[2] * srx))
		+ z*(ct[0] * sry - cry*(ct[2] * crx + ct[1] * srx));
	ht[4] = crz*(ct[4] * cry + sry*(ct[6] * crx + ct[5] * srx)) - srz*(ct[5] * crx - ct[6] * srx);
	ht[5] = srz*(ct[4] * cry + sry*(ct[6] * crx + ct[5] * srx)) + crz*(ct[5] * crx - ct[6] * srx);
	ht[6] = cry*(ct[6] * crx + ct[5] * srx) - ct[4] * sry;
	ht[7] = ct[7] - x*(crz*(ct[4] * cry + sry*(ct[6] * crx + ct[5] * srx)) - srz*(ct[5] * crx - ct[6] * srx))
		- y*(srz*(ct[4] * cry + sry*(ct[6] * crx + ct[5] * srx)) + crz*(ct[5] * crx - ct[6] * srx))
		+ z*(ct[4] * sry - cry*(ct[6] * crx + ct[5] * srx));
	ht[8] = crz*(ct[8] * cry + sry*(ct[10] * crx + ct[9] * srx)) - srz*(ct[9] * crx - ct[10] * srx);
	ht[9] = srz*(ct[8] * cry + sry*(ct[10] * crx + ct[9] * srx)) + crz*(ct[9] * crx - ct[10] * srx);
	ht[10] = cry*(ct[10] * crx + ct[9] * srx) - ct[8] * sry;
	ht[11] = ct[11] - x*(crz*(ct[8] * cry + sry*(ct[10] * crx + ct[9] * srx)) - srz*(ct[9] * crx - ct[10] * srx))
		- y*(srz*(ct[8] * cry + sry*(ct[10] * crx + ct[9] * srx)) + crz*(ct[9] * crx - ct[10] * srx))
		+ z*(ct[8] * sry - cry*(ct[10] * crx + ct[9] * srx));
	return;
}


//************************************
// Method:    frComputePose
// FullName:  frComputePose
// Access:    public 
// Returns:   int
// Qualifier:
// Parameter: STRUCT_POSE * reference,参考坐标系
// Parameter: STRUCT_POSE * source,原位姿
// Parameter: STRUCT_POSE * dist,相对于新的参考坐标系位姿
//************************************
int frComputePose(STRUCT_POSE *reference, STRUCT_POSE *source, STRUCT_POSE *dist)
{
	double source_t[12], dist_t[12];
	double rpy[3];
	source_t[3] = source->x;
	source_t[7] = source->y;
	source_t[11] = source->z;
	rpy[0] = source->rx;
	rpy[1] = source->ry;
	rpy[2] = source->rz;
	KineMath_RPY2T(source_t, rpy, 1);
	ClampT2HandT(reference, source_t, dist_t);
	KineMath_T2RPY(dist_t, rpy, 1);
	dist->x = dist_t[3];
	dist->y = dist_t[7];
	dist->z = dist_t[11];
	dist->rx = rpy[0];
	dist->ry = rpy[1];
	dist->rz = rpy[2];
	return 0;
}

