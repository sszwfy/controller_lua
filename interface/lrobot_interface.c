/********************************************************************
**   文 件 名:	lrobot_interface.c
**   Copyright   (c)   2017  杭州琦星机器人科技有限公司
**   创 建 人:	tangb 
**   日    期:	2017.4.18
**   修 改 人:	
**   功能描述:	
**   版    本:  v1.01
********************************************************************/
#ifdef linux
#include <pthread.h>
#include <unistd.h>
#else
#include <io.h>
#include <windows.h>
#endif
#include "lrobot.h"
#include "lrobot_interface.h"
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define LR_DEBUG	printf
#define LR_DEBUG(...) \
    do {              \
    } while (0)
#define CHECK_INIT() \
    if (lua_state == LR_NOINIT) return -1;
static LR_STATE lua_state = LR_NOINIT; //记录LUA脚本状态
static int8_t lua_filename[256];       //当前lua脚本文件名
static int32_t cur_line;
static int32_t break_line;
static lua_callback_msg lr_msg_callback = NULL;
//********************************************************************
//* 函数名称: interrupt
//* 函数描述: lua的hook回调函数
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static void interrupt(lua_State *L, lua_Debug *ar) {
    cur_line = ar->currentline;
    if (break_line != -1) //调试行
    {
        if (break_line == cur_line) {
            lua_state = LR_SUSPEND;
        }
    }
    LR_DEBUG("line=%d\r\n", ar->currentline);
LOOP:
    switch (lua_state) {
    case LR_SUSPEND:
#ifdef linux
        usleep(1000);
#else
        Sleep(1);
#endif
        goto LOOP;
    case LR_RESUME:
        lua_state = LR_RUNNING;
        break;
    case LR_STOP_SET:
        lua_sethook(L, NULL, 0, 0);
        lua_state = LR_STOPED;
        luaL_error(L, "interrupted!");
        break;
    case LR_EXITING:
        lua_sethook(L, NULL, 0, 0);
        luaL_error(L, "exit!");
        break;
    }
}

//********************************************************************
//* 函数名称: lrobot_do_event
//* 函数描述: robot类的事件回调
//* 输入参数:
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
static int xx = 0;
static int lrobot_do_event(LROBOT_EVENT_ID id, ...) {
    va_list ap;
    LUA_CALLBACK_PARAM p;
    va_start(ap, id);
    p.ret = 0;
    //LR_DEBUG("id=%d\r\n",id);
    switch (id) {
    case SERVO_ENABLE: {
        p.u.enable_servo = (int *) va_arg(ap, int *);
        LR_DEBUG("##SERVO_ENABLE %d, %d, %d, %d  ##\r\n", p.u.enable_servo[0], p.u.enable_servo[1], p.u.enable_servo[2], p.u.enable_servo[3]);
        lr_msg_callback(LUA_MSG_SERVO_ENABLE, (char *) &p);
        break;
    }
    case SERVO_STOP: {
        LR_DEBUG("##SERVO_STOP\r\n");
        lr_msg_callback(LUA_MSG_SERVO_STOP, 0);
        break;
    }
    case ROBOT_EXIT: {
        lua_state = LR_STOP_SET;
        LR_DEBUG("##SCRIPT_STOP\r\n");
        break;
    }
    case ROBOT_TYPE: {
        p.u.lr_type.devname = (const char *) va_arg(ap, const char *);
        p.u.lr_type.mio_id = (const char *) va_arg(ap, const char *);
        p.u.lr_type.gio_id = (const char *) va_arg(ap, const char *);
        LR_DEBUG("##ROBOT_TYPE %s,%s,%s\r\n", p.u.lr_type.devname, p.u.lr_type.mio_id, p.u.lr_type.gio_id);
        lr_msg_callback(LUA_MSG_SET_ROBOT_TYPE, (char *) &p);
        if (p.ret < 0) {
            lua_state = LR_STOP_SET;
        }
        break;
    }
    case REP_ERR:
    case REP_WARN:
    case REP_INFO: {
        p.u.rep_msg = (const char *) va_arg(ap, const char *);
        LR_DEBUG("##REP (%d): %s\r\n", id, p.u.rep_msg);
        lr_msg_callback(LUA_MSG_LOG_ERROR + id - REP_ERR, (char *) &p);
        break;
    }
    case MP_CP_POSE: {
        p.u.cp = (LUA_ROBOT_MP_CP *) va_arg(ap, LUA_ROBOT_MP_CP *);
        LR_DEBUG("##MP_CP_POSE\r\n");
        lr_msg_callback(LUA_MSG_MOVE_CP_POSE, (char *) &p);
        break;
    }
    case MP_SP_POSE: {
        int ret;
        LR_DEBUG("##MP_CP_POSE ST\r\n");
        p.u.sp = (LUA_ROBOT_MP_SP *) va_arg(ap, LUA_ROBOT_MP_SP *);
        lr_msg_callback(LUA_MSG_MOVE_SP_POSE, (char *) &p);
        while (lua_state == LR_RUNNING) //等待运动完成,如果出现其他问题,退出
        {
            lr_msg_callback(LUA_MSG_WAIT_MOVE_FINISHED, (char *) &ret);
            if (ret == 0) break; //运动完成退出
        }
        LR_DEBUG("##MP_CP_POSE ED\r\n");
        break;
    }
    case MP_CPT_POSE: {
        LR_DEBUG("##MP_CPT_POSE\r\n");
        p.u.cpt = (LUA_ROBOT_MP_CPT *) va_arg(ap, LUA_ROBOT_MP_CPT *);
        lr_msg_callback(LUA_MSG_MOVE_CPT_POSE, (char *) &p);
        break;
    }
    case MP_SPT_POSE: {
        int ret;
        LR_DEBUG("##MP_SPT_POSE ST\r\n");
        p.u.spt = (LUA_ROBOT_MP_SPT *) va_arg(ap, LUA_ROBOT_MP_SPT *);
        lr_msg_callback(LUA_MSG_MOVE_SPT_POSE, (char *) &p);
        while (lua_state == LR_RUNNING) //等待运动完成,如果出现其他问题,退出
        {
            lr_msg_callback(LUA_MSG_WAIT_MOVE_FINISHED, (char *) &ret);
            if (ret == 0) break; //运动完成退出
        }
        LR_DEBUG("##MP_SP_POSET ED\r\n");
        break;
    }
    case MP_WAIT: {
        LR_DEBUG("##MP_WAIT\r\n");
        lr_msg_callback(LUA_MSG_WAIT_MOVE_FINISHED, (char *) &p);
        break;
    }
    case MP_REF_POSE: {
        LR_DEBUG("##MP_REF_POSE\r\n");
        lr_msg_callback(LUA_MSG_REF_FRAME_POSE, (char *) &p); //设置参考坐标,参考坐标在lrobot模块有效,这里只传出消息,不传具体信息
        break;
    }
    case MIO_DOUT:
    case GIO_DOUT: {
        int t = id - MIO_DOUT;
        p.u.iout.channel = (int) va_arg(ap, int);
        p.u.iout.val = (int) va_arg(ap, int);
        //io out
        LR_DEBUG("##IOUT[%d] %d=%d\r\n", t, p.u.iout.channel, p.u.iout.val);
        lr_msg_callback(LUA_MSG_IO_SET_MIO_DO + t, (char *) &p);
        break;
    }
    case MIO_DIN:
    case GIO_DIN: {
        int t = id - MIO_DIN;
        p.u.ioin.channel = (int) va_arg(ap, int);
        p.u.ioin.val = (int *) va_arg(ap, int *);
        //io in IO状态传入val中
        lr_msg_callback(LUA_MSG_IO_RD_MIO_DI + t, (char *) &p);
        LR_DEBUG("##IOIN[%d] %d=%d\r\n", t, p.u.ioin.channel, *p.u.ioin.val);
        break;
    }
    case MIO_DOV:
    case GIO_DOV: {
        int t = id - MIO_DOV;
        p.u.dov = (STRUCT_DOV *) va_arg(ap, STRUCT_DOV *);
        LR_DEBUG("##DOV[%d] %d=%d\r\n", t, p.u.dov->channel, p.u.dov->mode);
        lr_msg_callback(LUA_MSG_DOV_SET_MIO + t, (char *) &p);
        break;
    }
    case MIO_DAC:
    case GIO_DAC: {
        int t = id - MIO_DAC;
        p.u.da = (STRUCT_DA *) va_arg(ap, STRUCT_DA *);
        LR_DEBUG("##DAC[%d] %d=%d\r\n", t, p.u.da->channel, p.u.da->mode);
        lr_msg_callback(LUA_MSG_DA_SET_MIO + t, (char *) &p);
        break;
    }
    case MIO_S_ADC:
    case GIO_S_ADC: {
        int t = id - MIO_S_ADC;
        p.u.ad = (STRUCT_AD *) va_arg(ap, STRUCT_AD *);
        LR_DEBUG("##S_ADC[%d] %d=%d\r\n", t, p.u.ad->channel, p.u.ad->sample_mode);
        lr_msg_callback(LUA_MSG_AD_SET_MIO + t, (char *) &p);
        break;
    }
    case MIO_G_ADC:
    case GIO_G_ADC: {
        int t = id - MIO_G_ADC;
        p.u.r_ad.channel = (int) va_arg(ap, int);
        p.u.r_ad.val = (float *) va_arg(ap, float *);
        lr_msg_callback(LUA_MSG_AD_RD_MIO + t, (char *) &p);
        LR_DEBUG("##G_ADC[%d] %d=%f\r\n", t, p.u.r_ad.channel, *p.u.r_ad.val);
        break;
    }
    case MIO_DOF:
    case GIO_DOF: {
        int t = id - MIO_DOF;
        p.u.dof = (STRUCT_DOF *) va_arg(ap, STRUCT_DOF *);
        LR_DEBUG("##DOF[%d] %d=%d\r\n", t, p.u.dof->channel, p.u.dof->mode);
        lr_msg_callback(LUA_MSG_DOF_SET_MIO + t, (char *) &p);
        break;
    }
    case MODEL_INDEX: {
        p.u.m_index = (STRUCT_MODEL_INFO *) va_arg(ap, STRUCT_MODEL_INFO *);
        LR_DEBUG("##INDEX[%d] %d,%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n", id, p.u.m_index->modelSeq, p.u.m_index->m_core_x, p.u.m_index->m_core_y,
                 p.u.m_index->rotateAngle, p.u.m_index->model_posit_x[0], p.u.m_index->model_posit_y[0], p.u.m_index->model_posit_x[1],
                 p.u.m_index->model_posit_y[1], p.u.m_index->model_posit_x[2], p.u.m_index->model_posit_y[2]);
        lr_msg_callback(LUA_MSG_READ_MODEL_INDEX, (char *) &p);
        break;
    }
    case SERVER_IP: {
        p.u.Ip = (const char *) va_arg(ap, const char *);
        LR_DEBUG("##Ip[%d] %s\r\n", id, p.u.Ip);
        lr_msg_callback(LUA_MSG_SET_SERVER_IP, (char *) &p);
        break;
    }
    case DLY_MS:
    case DLY_SEC: {
        //LR_DEBUG("##DELAY\r\n");
        if (lua_state != LR_RUNNING) p.ret = -1;
        break;
    }
    case CONNECT_MACHINE: {
        p.u.cm.context = (int *) va_arg(ap, int *);
        p.u.cm.ip = (const char *) va_arg(ap, const char *);
        lr_msg_callback(LUA_MSG_CONNECT_MACHINE, (char *) &p);
        break;
    }
    case DISCONNECT_MACHINE: {
        p.u.dcm.context = (int *) va_arg(ap, int *);
        lr_msg_callback(LUA_MSG_DISCONNECT_MACHINE, (char *) &p);
        break;
    }
    case SET_VIO: {
        p.u.set_vio.context = (int *) va_arg(ap, int *);
        p.u.set_vio.channel = (int) va_arg(ap, int);
        p.u.set_vio.val = (int) va_arg(ap, int);
        lr_msg_callback(LUA_MSG_SET_VIO, (char *) &p);
        break;
    }
    case READ_VIO: {
        p.u.read_vio.context = (int *) va_arg(ap, int *);
        p.u.read_vio.channel = (int) va_arg(ap, int);
        p.u.read_vio.val = (int *) va_arg(ap, int *);
        lr_msg_callback(LUA_MSG_READ_VIO, (char *) &p);
        break;
    }
    }
    va_end(ap);
    return p.ret;
}

#ifdef linux
static int lua_thread(void *arg)
#else
static DWORD WINAPI lua_thread(LPVOID pM)
#endif
{
    lua_State *L;
    L = luaL_newstate();
    if (L == NULL) {
        return -1;
    }
    luaL_openlibs(L);
    luaopen_robot(L, lrobot_do_event);
    while (1) {
        switch (lua_state) {
        case LR_IDLE:
#ifdef linux
            usleep(1000);
#else
            Sleep(1);
#endif
            break;
        case LR_LOADFILE: {
            int32_t bRet = luaL_loadfile(L, (const char *) lua_filename);
            cur_line = 0;
            if (bRet) {
                lr_msg_callback(LUA_WARN_SCRIPT_RUN_ERR, (char *) cur_line);
                lua_state = LR_ERR;
                break;
            }
            lua_sethook(L, interrupt, LUA_MASKLINE, 1);
            lua_state = LR_RUNNING;
            lr_msg_callback(LUA_MSG_SCRIPT_START, 0);
            bRet = lua_pcall(L, 0, 0, 0);
            if (bRet) {
                if ((lua_state != LR_STOPED) && (lua_state != LR_EXITING)) {
                    lua_state = LR_ERR;
                    lr_msg_callback(LUA_WARN_SCRIPT_RUN_ERR, (char *) cur_line);
                    break;
                }
                lr_msg_callback(LUA_WARN_SCRIPT_USER_STOP, 0);
            } else {
                lua_state = LR_STOPED;
                lr_msg_callback(LUA_MSG_SCRIPT_FINISHED, 0);
            }

            break;
        }
        case LR_STOPED:
            lua_state = LR_IDLE;
            break;
        }
        if (lua_state == LR_EXITING) break;
    }
    lua_close(L);
    lua_state = LR_EXIT;
    return 0;
}

//********************************************************************
//* 函数名称: lrobot_init
//* 函数描述: robot类的接口初始化,创建一个线程
//* 输入参数: msg_callback 事件及消息回调函数
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_init(lua_callback_msg msg_callback) {
#ifdef linux
    pthread_t id;
#endif
    if (msg_callback == NULL) {
        return -1;
    }
    lr_msg_callback = msg_callback;
    break_line = -1;
    cur_line = 0;
    lua_state = LR_IDLE;
    lua_filename[0] = 0;
#ifdef linux
    pthread_create(&id, NULL, (void *) lua_thread, NULL);
#else
    CreateThread(NULL, 0, lua_thread, NULL, 0, NULL);
#endif

    return 0;
}

//********************************************************************
//* 函数名称: lrobot_start
//* 函数描述: 启动lua脚本
//* 输入参数: lua_file,lua文件路径
//* 输出参数:
//* 返回说明: 0:成功,-1:当前状态不允许,-2,文件名为空,-3,文件不存在
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_start(char *lua_file) {
    CHECK_INIT();
    if (lua_state != LR_IDLE) {
        lr_msg_callback(LUA_WARN_STATE_NOT_PERMIT, "start not on idle");
        return -1;
    }
    if (lua_file == NULL) {
        lr_msg_callback(LUA_ERR_FILENAME, "filename=nil");
        return -2;
    }
#ifdef linux
    if (access(lua_file, 0) == 0) {
        break_line = -1;
        cur_line = 0;
        strncpy((char *) lua_filename, lua_file, 255);
        lua_state = LR_LOADFILE;
        return 0;
    }
#else
    if (_access(lua_file, 0) == 0) {
        break_line = -1;
        cur_line = 0;
        strcpy_s((char *) lua_filename, 255, lua_file);
        lua_state = LR_LOADFILE;
#ifdef linux
        usleep(100000);
#else
        Sleep(100);
#endif
        return 0;
    }
#endif
    lr_msg_callback(LUA_ERR_FILENAME, "file not exist");
    return -3;
}

//********************************************************************
//* 函数名称: lrobot_pause
//* 函数描述: 暂停lua脚本
//* 输入参数:
//* 输出参数:
//* 返回说明:  0,成功,-1,当前状态不允许(非运行状态)
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_pause(void) {
    CHECK_INIT();
    if (lua_state != LR_RUNNING) {
        lr_msg_callback(LUA_WARN_STATE_NOT_PERMIT, "pause not on running");
        return -1;
    }
    lua_state = LR_SUSPEND;
    return 0;
}

//********************************************************************
//* 函数名称: lrobot_continue
//* 函数描述: 继续lua脚本
//* 输入参数:
//* 输出参数:
//* 返回说明: 0,成功,-1,当前状态不允许(非运行状态)
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_continue(void) {
    CHECK_INIT();
    if (lua_state != LR_SUSPEND) {
        lr_msg_callback(LUA_WARN_STATE_NOT_PERMIT, "continue not on pause");
        return -1;
    }
    lua_state = LR_RESUME;
    return 0;
}

//********************************************************************
//* 函数名称: lrobot_stop
//* 函数描述: 停止lua脚本
//* 输入参数:
//* 输出参数:
//* 返回说明: 0,成功,-1,当前状态不允许(非运行状态或暂停状态)
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_stop(void) {
    CHECK_INIT();
    if ((lua_state < LR_RUNNING) || (lua_state > LR_RESUME)) {
        lr_msg_callback(LUA_WARN_STATE_NOT_PERMIT, "stop not on running");
        return -1;
    }
    lua_state = LR_STOP_SET;
    return 0;
}
//********************************************************************
//* 函数名称: lrobot_clearerr
//* 函数描述: 清除lua脚本运行错误(lua脚本语法错误或其他问题导致退出)
//* 输入参数:
//* 输出参数:
//* 返回说明: 0,成功,-1,当前状态不允许
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_clearerr(void) {
    CHECK_INIT();
    if (lua_state != LR_ERR) {
        lr_msg_callback(LUA_WARN_STATE_NOT_PERMIT, "no err");
        return -1;
    }
    lua_state = LR_IDLE;
    return 0;
}

//********************************************************************
//* 函数名称: lrobot_exit
//* 函数描述: 退出lua脚本解析线程,恢复需要重新调用lrobot_init
//* 输入参数:
//* 输出参数:
//* 返回说明: 0,成功
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_exit(void) {
    CHECK_INIT();
    lua_state = LR_EXITING;
    return 0;
}

//********************************************************************
//* 函数名称: lrobot_getstate
//* 函数描述: 获取lua脚本运行状态
//* 输入参数:
//* 输出参数:
//* 返回说明: 返回状态,查看LR_STATE定义
//* 其他:     18-4-2017, by tangb
//********************************************************************
LR_STATE lrobot_getstate(void) {
    return lua_state;
}

//********************************************************************
//* 函数名称: lrobot_setbreakline
//* 函数描述: 设置调试断点
//* 输入参数: line 调试行数  置 负数为关闭断点
//* 输出参数:
//* 返回说明: 0,成功
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_setbreakline(int line) {
    CHECK_INIT();
    if (line <= 0) {
        break_line = -1;
    }
    break_line = line;
    return 0;
}

//********************************************************************
//* 函数名称: lrobot_getrunline
//* 函数描述: 得到当前运行的行数
//* 输入参数:
//* 输出参数:
//* 返回说明: 返回行数
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_getrunline(void) {
    CHECK_INIT();
    return cur_line;
}
