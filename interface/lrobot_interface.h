#ifndef __LRROBOT_INTERFACE_H__
#define __LRROBOT_INTERFACE_H__
#include "lr_def.h"

//消息回调
typedef enum {
    //消息回调1000-1999
    LUA_MSG_SCRIPT_START = 1001, //脚本启动
    LUA_MSG_SCRIPT_FINISHED,     //脚本正常结束,流水脚本运行到结束
    LUA_MSG_SERVO_ENABLE,        //伺服使能
    LUA_MSG_SERVO_STOP,          //停止伺服
    LUA_MSG_SET_ROBOT_TYPE,      //设置机械臂类型

    //日志类错误码
    LUA_MSG_LOG_ERROR, //日志错误
    LUA_MSG_LOG_WARN,  //日志警告
    LUA_MSG_LOG_INFO,  //日志报告

    //点到点轨迹运动
    LUA_MSG_MOVE_CP_POSE = 1010, //点到点连续轨迹参数为pose运动
    LUA_MSG_MOVE_SP_POSE,        //点到点单指令运动参数为pose运动
    LUA_MSG_MOVE_CPT_POSE,       //点到点连续轨迹参数为posit运动
    LUA_MSG_MOVE_SPT_POSE,       //点到点单指令参数为posit运动
    LUA_MSG_REF_FRAME_POSE,      //设置坐标系
    LUA_MSG_WAIT_MOVE_FINISHED,  //等待运动结束

    //IO控制
    LUA_MSG_IO_SET_MIO_DO = 1020, //设置主IO板
    LUA_MSG_IO_SET_GIO_DO,        //设置工具IO板
    LUA_MSG_IO_RD_MIO_DI,         //读取主IO板
    LUA_MSG_IO_RD_GIO_DI,         //读取工具IO板
    LUA_MSG_DA_SET_MIO,           //设置DA主IO板
    LUA_MSG_DA_SET_GIO,           //设置DA工具IO板
    LUA_MSG_AD_SET_MIO,           //设置AD主IO板
    LUA_MSG_AD_SET_GIO,           //设置AD工具IO板
    LUA_MSG_AD_RD_MIO,            //读取AD主IO板
    LUA_MSG_AD_RD_GIO,            //读取AD工具IO板
    LUA_MSG_DOF_SET_MIO,          //设置DOF主IO板
    LUA_MSG_DOF_SET_GIO,          //设置DOF工具IO板
    LUA_MSG_DOV_SET_MIO,          //设置DOV主IO板
    LUA_MSG_DOV_SET_GIO,          //设置DOV工具IO板

    //vision
    LUA_MSG_READ_MODEL_INDEX = 1040, //读取模型序号
    LUA_MSG_SET_SERVER_IP,

    //VIO
    LUA_MSG_CONNECT_MACHINE = 1050, //连接机械臂
    LUA_MSG_DISCONNECT_MACHINE,     //断开机械臂链接
    LUA_MSG_SET_VIO,                //设置VIO
    LUA_MSG_READ_VIO,               //读取VIO

    LUA_WARN_STATE_NOT_PERMIT = 2001, //当前状态不允许此操作
    LUA_WARN_SCRIPT_USER_STOP,        //脚本用户停止结束或调用robot.exit退出
    LUA_WARN_SCRIPT_RUN_ERR,          //运行脚本时出现语法或其他错误
    //警告回调2000-2999
    LUA_ERR_FILENAME = 3001, //传入lua脚本路径错误

    //脚本函数名称错误
    //错误回调3000-3999

} LUA_CALLBACK_REASON;

typedef struct
{
    const char *devname;
    const char *mio_id;
    const char *gio_id;
} LUA_ROBOT_TYPE;
typedef struct
{
    int channel;
    int val;
} LUA_ROBOT_IO_OUT;

typedef struct
{
    int channel;
    int *val;
} LUA_ROBOT_IO_IN;

typedef struct
{
    int channel;
    float *val;
} LUA_ROBOT_IO_ADC;
typedef struct
{
    int modelSeq;
    float m_core_x;
    float m_core_y;
    float rotateAngle;
    float model_posit_x[3]; //模型坐标x
    float model_posit_y[3]; //模型坐标y
} LUA_MODEL_INDEX;

typedef struct {
    int *context;
    const char *ip;
} LUA_ROBOT_CONNECT_MACHINE;

typedef struct {
    int *context;
} LUA_ROBOT_DISCONNECT_MACHINE;

typedef struct {
    int *context;
    int channel;
    int val;
} LUA_ROBOT_SET_VIO;

typedef struct {
    int *context;
    int channel;
    int *val;
} LUA_ROBOT_READ_VIO;

typedef struct
{
    int ret;
    union {
        LUA_ROBOT_TYPE lr_type;
        LUA_ROBOT_MP_CP *cp;
        LUA_ROBOT_MP_SP *sp;
        LUA_ROBOT_MP_CPT *cpt;
        LUA_ROBOT_MP_SPT *spt;
        int *enable_servo;
        const char *rep_msg;
        LUA_ROBOT_IO_OUT iout;
        LUA_ROBOT_IO_IN ioin;
        STRUCT_DA *da;
        STRUCT_DOV *dov;
        STRUCT_AD *ad;
        STRUCT_DOF *dof;
        LUA_ROBOT_IO_ADC r_ad;
        STRUCT_MODEL_INFO *m_index;
        const char *Ip;
        LUA_ROBOT_CONNECT_MACHINE cm;
        LUA_ROBOT_DISCONNECT_MACHINE dcm;
        LUA_ROBOT_SET_VIO set_vio;
        LUA_ROBOT_READ_VIO read_vio;
    } u;
} LUA_CALLBACK_PARAM;

//lua消息回调函数
typedef int32_t (*lua_callback_msg)(int32_t reason, char *msg);

typedef enum {
    LR_NOINIT,   //未初始化
    LR_IDLE,     //空闲
    LR_LOADFILE, //加载lua文件
    LR_RUNNING,  //运行脚本中
    LR_SUSPEND,  //暂停
    LR_RESUME,   //恢复
    LR_STOP_SET, //设置停止
    LR_STOPED,   //已停止
    LR_ERR,      //脚本运行错误
    LR_EXITING,  //退出中
    LR_EXIT      //已退出,线程已完成,重新解析需要重新加载lrobot_init
} LR_STATE;
//********************************************************************
//* 函数名称: lrobot_init
//* 函数描述: robot类的接口初始化,创建一个线程
//* 输入参数: msg_callback 事件及消息回调函数
//* 输出参数:
//* 返回说明:
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_init(lua_callback_msg msg_callback);
//********************************************************************
//* 函数名称: lrobot_start
//* 函数描述: 启动lua脚本
//* 输入参数: lua_file,lua文件路径
//* 输出参数:
//* 返回说明: 0:成功,-1:当前状态不允许,-2,文件名为空,-3,文件不存在
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_start(char *lua_file);
//********************************************************************
//* 函数名称: lrobot_pause
//* 函数描述: 暂停lua脚本
//* 输入参数:
//* 输出参数:
//* 返回说明:  0,成功,-1,当前状态不允许(非运行状态)
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_pause(void);
//********************************************************************
//* 函数名称: lrobot_continue
//* 函数描述: 继续lua脚本
//* 输入参数:
//* 输出参数:
//* 返回说明: 0,成功,-1,当前状态不允许(非运行状态)
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_continue(void);
//********************************************************************
//* 函数名称: lrobot_stop
//* 函数描述: 停止lua脚本
//* 输入参数:
//* 输出参数:
//* 返回说明: 0,成功,-1,当前状态不允许(非运行状态或暂停状态)
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_stop(void);
//********************************************************************
//* 函数名称: lrobot_clearerr
//* 函数描述: 清除lua脚本运行错误(lua脚本语法错误或其他问题导致退出)
//* 输入参数:
//* 输出参数:
//* 返回说明: 0,成功,-1,当前状态不允许
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_clearerr(void);
//********************************************************************
//* 函数名称: lrobot_getstate
//* 函数描述: 获取lua脚本运行状态
//* 输入参数:
//* 输出参数:
//* 返回说明: 返回状态,查看LR_STATE定义
//* 其他:     18-4-2017, by tangb
//********************************************************************
LR_STATE lrobot_getstate(void);
//********************************************************************
//* 函数名称: lrobot_setbreakline
//* 函数描述: 设置调试断点
//* 输入参数: line 调试行数
//* 输出参数:
//* 返回说明: 0,成功
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_setbreakline(int line);
//********************************************************************
//* 函数名称: lrobot_getrunline
//* 函数描述: 得到当前运行的行数
//* 输入参数:
//* 输出参数:
//* 返回说明: 返回行数
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_getrunline(void);
//********************************************************************
//* 函数名称: lrobot_exit
//* 函数描述: 退出lua脚本解析线程,恢复需要重新调用lrobot_init
//* 输入参数:
//* 输出参数:
//* 返回说明: 0,成功
//* 其他:     18-4-2017, by tangb
//********************************************************************
int lrobot_exit(void);
#endif
